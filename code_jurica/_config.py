DATA_FOLDER='data/train/'
PREPARED_DATA_FOLDER='data/preprocesed/'

FR_HOME=DATA_FOLDER+'FR/training/raw/'
HU_HOME=DATA_FOLDER+'HU/training/raw/'
IT_HOME=DATA_FOLDER+'IT/training/raw/'

DATA_FR=FR_HOME+'corpus/'
DATA_HU=HU_HOME+'corpus/'
DATA_IT=IT_HOME+'corpus/'

TEST_FR = 'data/test/FR/raw/corpus/'
TEST_IT = 'data/test/IT/raw/corpus/'
TEST_HU = 'data/test/HU/raw/corpus/'

RESULTS_DIR = 'data/results/WBI/'

RESULTS = {
    'FR': {
        'in':TEST_FR + 'CausesBrutes_FR_2015F_1.csv',
        'out':RESULTS_DIR + 'FR/raw/'
        },
    'HU':{
        'in':TEST_HU + 'CausesBrutes_HU_2.csv',
        'out':RESULTS_DIR + 'HU/raw/'
    },
    'IT': {
        'in':TEST_IT + 'CausesBrutes_IT_2.csv',
        'out':RESULTS_DIR + 'IT/raw/'
        }
    }

TRAINING = {
    'FR': {
        'CB': [
            #DATA_FR + 'CausesBrutes_FR_2006-2012.csv',
            #DATA_FR + 'CausesBrutes_FR_2013.csv',
            DATA_FR + 'CausesBrutes_FR_2014.csv'
        ],
        'CC': [
            #DATA_FR + 'CausesCalculees_FR_2006-2012.csv',
            #DATA_FR + 'CausesCalculees_FR_2013.csv',
            DATA_FR + 'CausesCalculees_FR_2014.csv'
        ],
        'Ident': [
            DATA_FR + 'Ident_FR_2014.csv'
        ],
        "Encoding" : "latin1",
        "SplitMultiCode" : False
    },
    'HU': {
        'CB': [
            DATA_HU + 'CausesBrutes_HU_1.csv'
        ],
        'CC': [
            DATA_HU + 'CausesCalculees_HU_1.csv'
        ],
        'Ident': [
            DATA_HU + 'Ident_HU_1.csv'
        ],
        "Encoding" : "latin1",
        "SplitMultiCode" : False
    },
    'IT': {
        'CB': [
            DATA_IT + 'CausesBrutes_IT_1.csv'
        ],
        'CC': [
            DATA_IT + 'CausesCalculees_IT_1.csv'
        ],
        'Ident': [
            DATA_IT + 'Ident_IT_1.csv'
        ],
        "Encoding" : "latin1",
        "SplitMultiCode" : False
    }
}

DICT_FR=FR_HOME+'dictionaries/Dictionnaire2015.csv'
DICT_HU=HU_HOME+'dictionaries/Hungarian_dictionary_UTF8.csv'
DICT_IT=IT_HOME+'dictionaries/dictionary_IT.csv'

DICT_PREPROCESED=PREPARED_DATA_FOLDER+'dictionaries.p'
