# from comet_ml import Experiment
# experiment=Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj")

# from loader import *
import keras.backend as K
from util import *
import numpy as np
import random
import traceback
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from keras.layers import Embedding, Input, LSTM, Dense, Bidirectional
from keras.models import Model
from keras.utils import multi_gpu_model, np_utils

import tensorflow as tf

###################################
# TensorFlow wizardry
config=tf.ConfigProto()

# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type='BFC'

sess = tf.Session(graph=tf.get_default_graph(), config=config)
K.set_session(sess)

callbacks_list=[
    EarlyStopping(
        monitor='val_loss',
        patience=50,
    ),
    ModelCheckpoint(
        filepath='models/icd10Classification.h5',
        monitor='loss',
        save_best_only=True,
    ),
    CSVLogger(
        append=True,
        filename='logs/icd10Classification_{}.csv'.format(date_label),
    )
]

latent_dim = 512
epochs = 500
batch_size = 1000

tokenizer=TokenizePreprocessor()
kerasTokenizer = Tokenizer()
dataLoader=prepareData()
corpora=dataLoader.prepareDictionaries()
print("Extracted {} data points".format(len(corpora)))
# input('size of corpora')

#prepareing the texts for input in RNN
corpus=[x[0] for x in corpora]
tokens=tokenizer.transform([x for x in corpus])
tmp=[item for item in list(set(flatten(tokens))) if item.strip()]
vocabulary={item.strip():i+1 for i,item in enumerate(tmp)}
index_to_word_dict={i+1:item.strip() for i,item in enumerate(tmp)}
kerasTokenizer.word_index=vocabulary
# saving
with open('models/icd10_tokenizer.p', 'wb') as handle:
    pickle.dump(kerasTokenizer, handle)

source_word_sequence=kerasTokenizer.texts_to_sequences(corpus)
max_sequence = max([len(x) for x in source_word_sequence])
word_sequence = pad_sequences(source_word_sequence, maxlen=max_sequence, padding='post')

embedding_matrix=embedding_matrix(vocabulary)
embedding_layer = Embedding(
    embedding_matrix.shape[0],
    embedding_matrix.shape[1],
    weights=[embedding_matrix],
    input_length=max_sequence,
    trainable=True,
    mask_zero=True)

#preparing the labels as one hot encoding vector
labels=[x[1] for x in corpora]
num_labels=len(list(set(labels)))
print("Labels: {}\tUnique labels:{}".format(len(labels), num_labels))
encoder = LabelEncoder()
encoder.fit(labels)
with open('models/icd10_mappings.p', 'wb') as handle:
    pickle.dump(encoder, handle)

encoded_Y = encoder.transform(labels)

# convert integers to dummy variables (i.e. one hot encoded)
labels_one_hot = np_utils.to_categorical(encoded_Y)

X_train, X_test, Y_train, Y_test = train_test_split(word_sequence, labels_one_hot, test_size=0.05, random_state=777)
print("Prepared data: ", len(X_train), len(Y_train), len(X_test), len(Y_test))

try:
    # LAYERS
    print("Creating Model...")
    inputs = Input(shape=(max_sequence,))
    embedding = embedding_layer(inputs)
    decoder_LSTM = Bidirectional(LSTM(latent_dim))
    decoder_out = decoder_LSTM(embedding) #, initial_state=encoder_states)
    decoder_dense = Dense(num_labels, activation='softmax')
    decoder_out = decoder_dense(decoder_out)

    #MODEL
    model = Model(inputs=inputs, outputs=decoder_out)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    print("Traning Model...")
    model.fit(X_train, Y_train,
          batch_size=batch_size,
          epochs=epochs,
          callbacks=callbacks_list,
          validation_data=[X_test, Y_test]
          #verbose=0
    )

except Exception as e:
    print(e)
    traceback.print_exc()

