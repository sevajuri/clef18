# from comet_ml import Experiment
# experiment=Experiment(api_key="hSd9vTj0EfMu72569YnVEvtvj")

# from loader import *
from util import *
import numpy as np
import random
import traceback
from collections import Counter
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from itertools import chain

from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint, CSVLogger
from keras.layers import Embedding, Input, LSTM, Dense, Bidirectional
from keras.models import Model
from keras.utils import multi_gpu_model, np_utils

import tensorflow as tf

from _layers import AttentionWithContext, Attention


###################################
# TensorFlow wizardry
config=tf.ConfigProto()

# Don't pre-allocate memory; allocate as-needed
config.gpu_options.allow_growth=True
config.gpu_options.allocator_type='BFC'

callbacks_list=[
    EarlyStopping(
        monitor='val_loss',
        patience=2,
    ),
    ModelCheckpoint(
        filepath='models/icd10Classification_attention_char.h5',
        monitor='val_loss',
        save_best_only=True,
    ),
    CSVLogger(
        append=True,
        filename='logs/icd10Classification_attention_char_{}.csv'.format(date_label),
    )
]

latent_dim = 256
epochs = 100
batch_size = 1000

sentences = []
labels=[]
chars = set()

# tokenizer=TokenizePreprocessor()
kerasTokenizer = Tokenizer(char_level=True, filters=None)
dataLoader=prepareData()
corpora, labels_all =dataLoader.prepareDictionaries()
labels_c = Counter(labels_all)
labels_tmp = {k:v for k,v in labels_c.items() if v > 1}

# input('size of corpora')

for line, label in zip(corpora, labels_all):
    if label in labels_tmp:
        sentences.append(line)
        labels.append(label)
        tmp =  set(line)
        chars  = set(chain(chars,tmp))

# print(chars, len(chars))
chars = sorted(list(chars))
char_to_index_dict={item.strip():i+1 for i,item in enumerate(chars)}
index_to_char_dict={i+1:item.strip() for i,item in enumerate(chars)}

kerasTokenizer.word_index=char_to_index_dict
# saving
with open('models/icd10_char_tokenizer.p', 'wb') as handle:
    pickle.dump(kerasTokenizer, handle)

char_sequence=kerasTokenizer.texts_to_sequences(sentences)
# print(char_sequence)
max_sequence = max([len(x) for x in char_sequence])
word_sequence = pad_sequences(char_sequence, maxlen=max_sequence, padding='post')

embedding_matrix=embedding_matrix(char_to_index_dict)
embedding_layer = Embedding(
    embedding_matrix.shape[0],
    embedding_matrix.shape[1],
    weights=[embedding_matrix],
    input_length=max_sequence,
    trainable=True,
    mask_zero=True)

#preparing the labels as one hot encoding vector

num_labels=len(list(set(labels)))
print("Labels: {}\tUnique labels:{}".format(len(labels), num_labels))
encoder = LabelEncoder()
encoder.fit(labels)
with open('models/icd10_char_mappings.p', 'wb') as handle:
    pickle.dump(encoder, handle)

encoded_Y = encoder.transform(labels)

# convert integers to dummy variables (i.e. one hot encoded)
labels_one_hot = np_utils.to_categorical(encoded_Y)
print(word_sequence.shape, labels_one_hot.shape)
print(type(word_sequence), type(labels_one_hot))

input('bla')

X_train, X_test, Y_train, Y_test = train_test_split(word_sequence, labels_one_hot, test_size=0.05, random_state=777, stratify=labels)
print("Prepared data: ", len(X_train), len(Y_train), len(X_test), len(Y_test))

try:
    # LAYERS
    print("Creating Model...")
    inputs = Input(shape=(max_sequence,))
    embedding = embedding_layer(inputs)
    decoder_LSTM = Bidirectional(LSTM(latent_dim, return_sequences=True))
    decoder_out = decoder_LSTM(embedding) #, initial_state=encoder_states)
    attention = Attention()(decoder_out)
    decoder_dense = Dense(num_labels, activation='softmax')
    decoder_out = decoder_dense(attention)

    #MODEL
    model = Model(inputs=inputs, outputs=decoder_out)
    # model = multi_gpu_model(tmp_model, gpus=2)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])
    model.summary()
    print("Traning Model...")
    model.fit(X_train, Y_train,
          batch_size=batch_size,
          epochs=epochs,
          callbacks=callbacks_list,
          validation_split=0.25
    )

except Exception as e:
    print(e)
    traceback.print_exc()

