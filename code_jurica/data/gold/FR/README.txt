The CepiDC Corpus is released to CLEFeHealth 2018 task 1 participants who have signed the data user agreement. 


========================
CLEF e-Health 2018 Task 1, CepiDC
========================

The CLEF e-Health 2018 Task 1 CepiDC data comprises the following:

Gold Standard Test data: 

* The text of 11,932 death certificates and associated gold standard ICD10 codes. 

(GoldStandardFR2008_IDs.out)
* The ID list of 11,932 death certificates used in this evaluation. 

(AlignedCauses_2015_full_2018_UTF8_filtered_1m_commonRaw.csv)
The corpus includes raw text from the death certificates along with selected metadata: 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntType: type of time interval the patient had been suffering from coded cause, according to the following categories: 
    1 => minutes
    2 => hours
    3 => days
    4 => months
    5 => years  
* IntValue: length of time the patient had been suffering from coded cause; for example, if the patient had been experiencing the cause for 6 months, "IntValue" should be 6 and "IntType" should be 4.  
 
(CausesCalculees_FR_2015_F_1_commonAligned.csv). 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
---
* CauseRank: Rank of the ICD10 code assigned by coder
* StandardText: dictionary entry or exerpt of the raw text that supports the selection of an ICD10 code (if any)
* ICD10: gold standard ICD10 code
 


