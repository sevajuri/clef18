The CépiDC Corpus is released to CLEFeHealth 2018 task 1 participants who have signed the user agreement available at https://www.google.com/url?q=https%3A%2F%2Focsync.limsi.fr%2Findex.php%2Fs%2FbkXzQKEDyYlY3fR&sa=D&sntz=1&usg=AFQjCNEeJ2aA973BfVcAyDLHHS3fxg1b8g
 

========================
CLEF e-Health 2018 Task 1
========================

The CLEF e-Health 2018 Task 1 CépiDC (French, document-level) test data comprises the following:

- corpus folder: 

Data from 24375 death certificates and selected metadata distributed over the following two files: 

(CausesBrutes_FR_2015F_1.csv) 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntType: type of time interval the patient had been suffering from coded cause, according to the following categories: 
    1 => minutes
    2 => hours
    3 => days
    4 => months
    5 => years  
* IntValue: length of time the patient had been suffering from coded cause; for example, if the patient had been experiencing the cause for 6 months, "IntValue" should be 6 and "IntType" should be 4.  

(Ident_FR_2015F_1.csv)
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* PrimCauseCode: the code of the primary cause of death (redacted from this test set)
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location


