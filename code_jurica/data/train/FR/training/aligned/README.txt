The CepiDC Corpus is released to CLEFeHealth 2018 task 1 participants who have signed the user agreement available at *URL TBA*


========================
CLEF e-Health 2018 Task 1, CepiDC
========================

The CLEF e-Health 2018 Task 1 CepiDC Gold Standard Training data comprises the following:


- corpus folder: 

Training Data

The text of 65,843 death certificates and associated gold standard ICD10 codes (AlignedCauses_2006-2012.csv). 
The corpus includes raw text from the death certificates along with selected metadata: 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntType: type of time interval the patient had been suffering from coded cause, according to the following categories: 
    1 => minutes
    2 => hours
    3 => days
    4 => months
    5 => years  
* IntValue: length of time the patient had been suffering from coded cause; for example, if the patient had been experiencing the cause for 6 months, "IntValue" should be 6 and "IntType" should be 4.  
---
* CauseRank: Rank of the ICD10 code assigned by coder
* StandardText: dictionary entry or exerpt of the raw text that supports the selection of an ICD10 code
* ICD10: gold standard ICD10 code
 
(Note that the last three fields will not be supplied in the test set. ICD10 codes will be expected from participants. Cause Rank and Standard Text may be supplied by participants if desired. Examples of the supplied input vs. expected output formats are provided as development data)
 

Development Data: 
* The text of 27,850 death certificates and associated gold standard ICD10 codes in the training format (AlignedCauses_2013full.csv - see format description above). The text of 27,850 death certificates in the input test format, without gold standard codes (AlignedCauses_2013.csv). 
* The text of 31,690 death certificates and associated gold standard ICD10 codes in the training format (AlignedCauses_2014full.csv). The text of 31,690 death certificates in the input test format, without gold standard codes (AlignedCauses_2014.csv). 

The corpus includes raw text from the death certificates along with selected metadata: 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntType: type of time interval the patient had been suffering from coded cause, according to the following categories: 
    1 => minutes
    2 => hours
    3 => days
    4 => months
    5 => years  
* IntValue: length of time the patient had been suffering from coded cause; for example, if the patient had been experiencing the cause for 6 months, "IntValue" should be 6 and "IntType" should be 4.  
 

- dictionary folder: 6 versions of a manually curated ICD10 dictionary developped at CépiDC.   
- eval folder: the  evaluation tool developped for CLEF e-Health 2016 Task 2, clefehealth2017Task1_eval.pl
sample use: 
perl eval/clefehealth2017Task1_eval.pl.pl
(will print usage information)
perl eval/clefehealth2017Task1_eval.pl corpus/AlignedCauses_2006-2012.csv corpus/AlignedCauses_2006-2012.csv -c 
(will print detailed performance per category with perfect scores)

- PavillonLaurent.pdf: a document (in French) presenting the original paper form used to collect death certificate text such as contained in this corpus - see Figure 1. The document also explain the coding rules applied to extract ICD10 codes from the certificate text.   


