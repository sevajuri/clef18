The CepiDC Corpus is released to CLEFeHealth 2018 task 1 participants who have signed the user agreement available at *URL TBA*
 

========================
CLEF e-Health 2018 Task 1, CepiDC
========================

The CLEF e-Health 2018 Task 1 CepiDC (French, document-level) training data comprises the following:

- corpus folder:

Training Data 

The text of 65,843 death certificates, selected metadata and associated gold standard ICD10 codes, distributed over the following three files: 

(CausesBrutes_FR_2006-2012.csv) 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntType: type of time interval the patient had been suffering from coded cause, according to the following categories: 
    1 => minutes
    2 => hours
    3 => days
    4 => months
    5 => years  
* IntValue: length of time the patient had been suffering from coded cause; for example, if the patient had been experiencing the cause for 6 months, "IntValue" should be 6 and "IntType" should be 4.  

(Ident_FR_2006-2012.csv)
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* PrimCauseCode: the code of the primary cause of death
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location

(Note that the primary cause of death will not be supplied in the test set)

(CausesCalculees_FR_2006-2012.csv). 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
---
* CauseRank: Rank of the ICD10 code assigned by coder
* StandardText: dictionary entry or exerpt of the raw text that supports the selection of an ICD10 code (if any)
* ICD10: gold standard ICD10 code
 
(Note that this file will not be supplied in the test set, as participant will be expected to create it. DocID, YearCoded and LineID will be expected to be carried over from the CauseBrutes file. ICD10 codes will be expected from the participants' automatic prediction. Cause Rank and Standard Text may be supplied by participants if desired; fields may otherwise be left blank)

Development Data

* The text of 27,850 death certificates (CausesBrutes_FR_2013.csv - see format description above).
Metadata for the 27,850 death certificates in the training format (Ident_FR_2013_full.csv - see format description above) and in the test format (Ident_FR_2013.csv - primary causes will be redacted). Gold standard ICD10 codes for the 27,850 death certificates in the format that participants are expected to supply (CausesCalculees_FR_2013.csv).  

* The text of 31,683 death certificates (CausesBrutes_FR_2014.csv - see format description above).
Metadata for the 31,683 death certificates in the training format (Ident_FR_2014_full.csv - see format description above) and in the test format (Ident_FR_2014.csv - primary causes will be redacted). Gold standard ICD10 codes for the 27,850 death certificates in the format that participants are expected to supply (CausesCalculees_FR_2014.csv).  


- dictionary folder: 6 versions of a manually curated ICD10 dictionary developped at CépiDC.   
- eval folder: the document-level evaluation tool developped for CLEF e-Health 2018 Task 1, clefehealthTask12017_plainCertifeval.pl
sample use: 
perl eval/clefehealthTask12017_plainCertifeval.pl
(will print usage information)
perl eval/clefehealthTask12017_plainCertifeval.pl corpus/train/CausesCalculees_FR_training.csv corpus/train/CausesCalculees_FR_training.csv -d
(will print detailed performance per certificate with perfect scores)


