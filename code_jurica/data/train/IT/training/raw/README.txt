The CepiDC Corpus is released to CLEFeHealth 2018 task 1 participants who have signed the user agreement available at https://www.google.com/url?q=https%3A%2F%2Focsync.limsi.fr%2Findex.php%2Fs%2FbkXzQKEDyYlY3fR&sa=D&sntz=1&usg=AFQjCNEeJ2aA973BfVcAyDLHHS3fxg1b8g
 

========================
CLEF e-Health 2018 Task 1, CepiDC
========================

The CLEF e-Health 2018 Task 1 CepiDC (Italian, document-level) training data comprises the following:

- corpus folder:

Training Data 

The text of 14,502 death certificates, selected metadata and associated gold standard ICD10 codes, distributed over the following three files: 

(CausesBrutes_IT_1.csv) 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
* RawText: raw text entered in the death certificate
* IntervalText: free text describing the time interval the patient had been suffering from coded cause. 

(Ident_IT_1.csv)
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* Gender: gender of the deceased 
* PrimCauseCode: the code of the primary cause of death
* Age: age at the time of death, rounded to the nearest five-year age group 
* LocationOfDeath: Location of death, according to the following categories:
		1 => Home
		2 => Hospital
		3 => Private Clinic
		4 => Hopice, Retirement home
		5 => Public place
		6 => Other Location

(Note that the primary cause of death will not be supplied in the test set)

(CausesCalculees_IT_1.csv). 
* DocID: death certificate ID
* YearCoded: year the death certificate was processed by CépiDC
* LineID: line number within the death certificate
---
* CauseRank: Rank of the ICD10 code assigned by coder
* StandardText: dictionary entry or exerpt of the raw text that supports the selection of an ICD10 code (if any)
* ICD10: gold standard ICD10 code
* IntervalText: free text describing the time interval the patient had been suffering from coded cause. 
 
(Note that this file will not be supplied in the test set, as participant will be expected to create it. DocID, YearCoded and LineID will be expected to be carried over from the CauseBrutes file. ICD10 codes will be expected from the participants' automatic prediction. Cause Rank and Standard Text may be supplied by participants if desired; fields may otherwise be left blank)


- dictionary folder: a manually curated ICD10 dictionary
- eval folder: the document-level evaluation tool developped for CLEF e-Health 2018 Task 1, clefehealthTask12017_plainCertifeval.pl
sample use: 
perl eval/clefehealthTask12017_plainCertifeval.pl
(will print usage information)
perl eval/clefehealthTask12017_plainCertifeval.pl corpus/CausesCalculees_IT_1.csv corpus/CausesCalculees_IT_1.csv -dm
(will print detailed performance per certificate with perfect scores)

- Certificate_Italian.pdf: a copy of the Italian certificate of death document used to collect the data in this dataset. 
