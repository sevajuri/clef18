#!/usr/local/bin/perl -w


if($#ARGV < 1){
 print "Usage: $0 REFfile TESTfile OPT\n";
 print "\tOPT=-c for evaluation per CATEGORY (default OVERALL)\n";
 print "\tOPT=-d for evaluation per DOCUMENT line (default OVERALL)\n";
 print "\tOPT=-e perform additional evaluation for EXTERNAL CAUSES (only in combination with CATEGORY evaluation)\n";
 print "\tOPT=-i for case insensitive evaluation\n";
 print "\tOPT=-m for verbose Messages\n";
 print "\tOPT=-s for Super verbose messages\n";
 print "\tFORMAT - per line: DocID;YearCoded;LineID;Rank;StandardText;ICD10\n";

}else{

  my ($REFfile,$TESTfile,$OPT) = (@ARGV);
  my %CAT_relevant_H=();
  my ($id,$text,$dic,$gs);
  if ($OPT eq ""){$OPT="n";} # no options
  if($OPT =~/i/){print "CASE INSENSITIVE PROCESSING\n";}
  
# collect REF info

  my ($gs_HoHref,$CATgs_HoHref,$relevant_Href,$CAT_relevant_Href)=load($REFfile,$OPT);
  
# read TEST data

my ($test_HoHref,$CATtest_HoHref,$retrieved_Href,$CAT_retrieved_Href)=load($TESTfile,$OPT);

## compute performance measures

# performance by certificate

  if($OPT =~/d/){
      my $docOPT=$OPT; $docOPT=~s/c//; $docOPT=~s/e//;
      performance($gs_HoHref,$test_HoHref,$retrieved_Href,$relevant_Href,$docOPT);
  }

# performance by category
    
    if($OPT =~/c/){
	my $catOPT=$OPT; $catOPT=~s/d//;
	performance($CATgs_HoHref,$CATtest_HoHref,$CAT_retrieved_Href,$CAT_relevant_Href,$catOPT);
    }

} # end else

sub load{
    my ($file,$OPT)= @_;
    my $c=0; my $all=0;
    my $id;
    my %doc_H=(); my %cat_H=(); my %doccat_HoH=(); my %catdoc_HoH=();
     open(IN,"$file") || die "cannot open REFfile *$file* for reading\n";
     while(<IN>){
     	chomp;
        next if ($. == 1);		# ADDED THIS LINE
 	my $line=$_;
	$all++;
	my ($docid,$year,$lineid,$rank,$text,$gs)=split(/\;/,$line);

	    $id=$docid;
	
	if($OPT=~/i/){$gs=~tr/[A-Z]/[a-z]/;}
     	
    	if($OPT =~/s/) { if($docid eq "14"){print "reading from $file : *$gs* for *$id* @ line $line\n";} }
	if($OPT =~/s/){print "reading from $file : *$gs* for *$id* @ line $line\n";}
# CHANGED     	if($gs ne ""){
     	if($gs ne "" && $gs ne "NULL"){
	    $c++;
	    unless($doccat_HoH{$id}{$gs}){
		$doccat_HoH{$id}{$gs}++;
		$doc_H{$id}++;
		if($OPT =~/s/) { if($docid eq "14"){print "\t->loading : *$gs* for *$id*\n";} }
		if($OPT =~/s/){print "\t->loading : *$gs* for *$id*\n";}
	    }else{
		##  DBG ## print "DUPLICATE LINE: $line\n";
	    }
	    unless($catdoc_HoH{$gs}{$id}){
		$catdoc_HoH{$gs}{$id}++;
		$cat_H{$gs}++;
	    }else{
		##  DBG ##		print "DUPLICATE LINE: $line\n";
	    }
	}else{
##  DBG ##	    print "NOT LOADED::$all:: $line\n"; 
	}

    } # end while
    close(IN);					# ADDED THIS LINE
     
  my $refcat_count = keys %cat_H;

    print "Loaded $c lines out of $all from file $file\n\n";
    print "Loaded $refcat_count ref categories:\n";
    if( ($OPT =~/m/) && ($OPT =~/c/) ){
      foreach my $cat (keys %cat_H){
    print "\t<$cat> ($cat_H{$cat} occurrences)\n";
      }
    }
    print "\n";
    
    $c=0;

    return (\%doccat_HoH,\%catdoc_HoH,\%doc_H,\%cat_H);
}


sub performance{
    my ($gs_HoHref,$test_HoHref,$retrieved_Href,$relevant_Href,$OPT)=@_;

    my %test_rel_ret_HoH=();
    my $ALLretrieved=0;
    my $ALLrelevant=0;
    my $ALLrel_ret=0;
    my $cptid=0;  my $cptEid=0;

  foreach $id (keys %{$gs_HoHref}){
      if($OPT =~/s/){print "\nreading GS for $id\n";}
      my $rel_ret=0;
      
      
      foreach $code (keys %{$gs_HoHref->{$id}}){
	  if($OPT =~/s/){print "\t->found code $code";}
	  if($test_HoHref->{$id}->{$code}){
	      $rel_ret++; 
	      if($OPT =~/s/){print "\t->-> RETRIEVED\n";}
	      $test_rel_ret_HoH{$id}{$code}++; 
	  }else{if($OPT =~/s/){print "\t->-> NOT RETRIEVED\n";}}
      }

      
	  $cptid++;
      # local id performance
      
      if($retrieved_Href->{$id}){$p=$rel_ret/$retrieved_Href->{$id};}else{$p=0;}
      if($relevant_Href->{$id}){$r=$rel_ret/$relevant_Href->{$id};}else{$r=0;}
      if($p && $r){$f=2*$p*$r/($p+$r);}else{$f=0;}
 
      # external causes performance
      if($id =~/[VWXYvwxy]/){
	   if($rel_ret){$Erel_ret+=$rel_ret; }
	   if($relevant_Href->{$id}){$Erelevant+=$relevant_Href->{$id};}
	   if($retrieved_Href->{$id}){$Eretrieved+=$retrieved_Href->{$id};}
	   $cptEid++;
      }
      
      if($rel_ret){$ALLrel_ret+=$rel_ret; }
      if($relevant_Href->{$id}){$ALLrelevant+=$relevant_Href->{$id};}
      if($retrieved_Href->{$id}){$ALLretrieved+=$retrieved_Href->{$id};}
      
      my @relret_A=keys %{$test_rel_ret_HoH{$id}};
      my @rel_A=keys %{$gs_HoHref->{$id}};
      my @ret_A=keys %{$test_HoHref->{$id}};
      
      
      if($OPT =~/m/){
	  printf "$id|P %2.2f|R %2.2f|F %2.2f\n",$p,$r,$f;
      }

      if($OPT =~/e/){
	  if($id =~/[VWXYvwxy]/){
	      printf "$id|P %2.2f|R %2.2f|F %2.2f\n",$p,$r,$f;
       }	  
      }

      
      if($OPT =~/s/){
	  print "\t(rel_ret $rel_ret -> @relret_A, rel $relevant_Href->{$id} -> @rel_A, ret $retrieved_Href->{$id} -> @ret_A)\n";
      }
  }

  # ADDED THE FOREACH LOOP BELOW
  foreach $id (keys %{$test_HoHref}) {
    if (!$gs_HoHref->{$id}) {
      $ALLretrieved += $retrieved_Href->{$id};
    }
  }
  
  if($ALLretrieved){$ALLp=$ALLrel_ret/$ALLretrieved;}else{$ALLp=0;}
  if($ALLrelevant){$ALLr=$ALLrel_ret/$ALLrelevant;}else{$ALLr=0;}
    if($ALLp && $ALLr) {$ALLf=2*$ALLp*$ALLr/($ALLp+$ALLr);}else{$ALLf=0;}

  if($Eretrieved){$Ep=$Erel_ret/$Eretrieved;}else{$Ep=0;}
  if($Erelevant){$Er=$Erel_ret/$Erelevant;}else{$Er=0;}
    if($Ep && $Er) {$Ef=2*$Ep*$Er/($Ep+$Er);}else{$Ef=0;}

    
# CHANGED    printf "ALL|P %2.2f|R %2.2f|F %2.2f\n",$ALLp,$ALLr,$ALLf;
    printf "ALL|P %.4f|R %.4f|F %.4f\n",$ALLp,$ALLr,$ALLf;
    print "\t(rel_ret $ALLrel_ret, rel $ALLrelevant, ret $ALLretrieved)\n";
    if($OPT =~/c/){ print "(results for $cptid TEST CATEGORIES)\n";}
    if($OPT =~/d/){ print "(results for $cptid TEST DOCUMENTS)\n";}
    
    if($OPT =~/e/){
	printf "\nEXTERNAL|P %.4f|R %.4f|F %.4f\n",$Ep,$Er,$Ef;
	print "\t(rel_ret $Erel_ret, rel $Erelevant, ret $Eretrieved)\n";
	print "(results for $cptEid EXTERNAL CAUSES CATEGORIES)\n";
    }
   
 
}
