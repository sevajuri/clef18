# import pubmed_parser as pp
from pubmed_parser.utils import read_xml, stringify_children
import pickle
from os import listdir
from os.path import isfile, join
from multiprocessing import Pool, cpu_count  #Queue, Process, Manager, current_process, active_children, # use threads
import tqdm

language_codes = ['ita', 'fre', 'hun']

path='/home/docClass/files/pubmed/pubmed18n1064.xml.gz'

# def read_xml(path):
#     """
#     Parse tree from given XML path
#     """
#     try:
#         tree = etree.parse(path)
#     except:
#         try:
#             tree = etree.fromstring(path)
#         except Exception as e:
#             print("Error: it was not able to read a path, a file-like object, or a string as an XML")
#             raise
#     if '.nxml' in path:
#         remove_namespace(tree) # strip namespace for
#     return tree
#
#
# def stringify_children(node):
#     """
#     Filters and removes possible Nones in texts and tails
#     ref: http://stackoverflow.com/questions/4624062/get-all-text-inside-a-tag-in-lxml
#     """
#     parts = ([node.text] +
#              list(chain(*([c.text, c.tail] for c in node.getchildren()))) +
#              [node.tail])
#     return ''.join(filter(None, parts))

def findOtherAbstracts(medline, languages):
    otherAbstracts={}
    article = medline.find('OtherAbstract')
    if article:
        # print(article)
        if article.attrib['Language'] in languages:
            print(stringify_children(article))
            otherAbstracts[article.attrib['Language']]=[stringify_children(article)]
    return otherAbstracts

def extractOtherAbstract(path):
    otherAbstracts = {l: [] for l in language_codes}
    tree = read_xml(path)
    medline_citations = tree.findall('//MedlineCitationSet/MedlineCitation')
    if len(medline_citations) == 0:
        medline_citations = tree.findall('//MedlineCitation')

    for mc in medline_citations:
        article = mc.find('OtherAbstract')
        if article is not None:
        #print(article)
            if article.attrib['Language'].strip() in otherAbstracts:
                # print(stringify_children(article))
                otherAbstracts[article.attrib['Language']].append(stringify_children(article))

    # start_time = time.clock()
    # article_list = list(map(lambda m: findOtherAbstracts(m), medline_citations))
    # print(time.clock() - start_time, "seconds")

    return otherAbstracts

print("Prepraing Input Files")
PubMed_DLPath='/home/docClass/files/pubmed/'
inputFiles = []
inputFiles.extend([PubMed_DLPath + files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))])
#
other_languages={l: [] for l in language_codes}
p = Pool(processes=cpu_count())
for res in tqdm.tqdm(p.imap_unordered(extractOtherAbstract, inputFiles), total=len(inputFiles)):
    for i, j in res.items():
        other_languages[i].extend(j)

for k,v in other_languages.items():
    print(k,len(v))

pickle.dump(other_languages, open('data/embeddings/otherLanguages.p', 'wb'))

