from util import *
import numpy as np
import random
from sklearn.model_selection import train_test_split
import pickle
import os

from keras.preprocessing.text import Tokenizer
from keras.layers import Embedding

#REPRODUCIBLE
os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(42)
random.seed(12345)

source_kerasTokenizer = Tokenizer()
target_kerasTokenizer = Tokenizer()
tokenizer = TokenizePreprocessor()
prepareData = prepareData()
SEED = 777

itCorpora, itErrors = prepareData.prepareData('IT')
huCorpora, huErrors = prepareData.prepareData('HU')
frCorpora, frErrors = prepareData.prepareData('FR')
# print(len(frErrors), len(itErrors), len(huErrors))


try:
    df = pd.DataFrame(frErrors+itErrors+huErrors, columns=['Dataset','DocID', 'MissingRowID'])
    df.to_csv('Errors.csv')
except Exception as e:
    print(e)

# min_elements=min(len(frCorpora),len(itCorpora),len(huCorpora))
# FR_sample=random.sample(frCorpora, min_elements)
# IT_sample=random.sample(itCorpora, min_elements)
# HU_sample=random.sample(huCorpora, min_elements)
# corpora=FR_sample+HU_sample+IT_sample

corpora=frCorpora+itCorpora+huCorpora
# print(len(corpora))
# corpora=corpora[:int(len(corpora)*0.5)]
# print(len(corpora))
# input('bla')

#labels - icd10 codes
labels=[str(x[2]).strip() for x in corpora]

#SOURCE TOKENS
# source_corpus=['sos '+x[0]+' eos' for x in corpora]
source_corpus=[x[0] for x in corpora]
source_tokens = tokenizer.transform([x for x in source_corpus])
source_max_sequence_tokenizer = max([len(x) for x in source_tokens])
tmp =[item for item in list(set(flatten(source_tokens))) if item.strip()]
source_vocab = {item.strip():i+1 for i,item in enumerate(tmp)}
source_index_to_word_dict = {i+1:item.strip() for i,item in enumerate(tmp)}
source_kerasTokenizer.word_index=source_vocab
with open('models/s2s_source_tokenizer_extended.p', 'wb') as handle:
    pickle.dump(source_kerasTokenizer, handle)

#TARGET TOKENS
target_corpus=['sos '+x[1]+' eos' for x in corpora]
target_tokens = tokenizer.transform([x for x in target_corpus])
target_max_sequence_tokenizer = max([len(x) for x in target_tokens])
tmp=[item for item in list(set(flatten(target_tokens))) if item.strip()]
target_vocab = {item.strip():i+1 for i,item in enumerate(tmp)}
target_index_to_word_dict = {i+1:item.strip() for i,item in enumerate(tmp)}
target_kerasTokenizer.word_index=target_vocab
with open('models/s2s_target_tokenizer_extended.p', 'wb') as handle:
    pickle.dump(target_kerasTokenizer, handle)


# source_word_sequence=kerasTokenizer.texts_to_sequences(source_corpus)
# source_max_sequence = max([len(x) for x in source_word_sequence])
# source_word_sequence = pad_sequences(source_word_sequence, maxlen=source_max_sequence, padding='post')
# print('See source lengths: {} {}'.format(source_max_sequence_tokenizer, source_max_sequence))

# target_word_sequence=kerasTokenizer.texts_to_sequences(target_corpus)
# target_max_sequence = max([len(x) for x in target_word_sequence])
# target_word_sequence = pad_sequences(target_word_sequence, maxlen=target_max_sequence, padding='post')
# print('See source lengths: {} {}'.format(target_max_sequence_tokenizer, target_max_sequence))
# input("rawwwww")

# print("Source vocabulary: {}".format(len(source_vocab)))
# print(source_word_sequence.shape)
# print("Target vocabulary: {}".format(len(target_vocab)))
# print(target_word_sequence.shape)
# frCorpora = itCorpora= huCorpora= frErrors= itErrors= huErrors = corpora=None

#EMBEDDINGS FORM FASTTEXT
source_embeddings=embedding_matrix(source_vocab)
source_embedding_layer = Embedding(source_embeddings.shape[0],
                                   source_embeddings.shape[1],
                                   weights=[source_embeddings],
                                   input_length=source_max_sequence_tokenizer,
                                   trainable=True,
                                   mask_zero=True)

target_embeddings=embedding_matrix(target_vocab)
target_embedding_layer = Embedding(target_embeddings.shape[0],
                                   target_embeddings.shape[1],
                                   weights=[target_embeddings],
                                   input_length=target_max_sequence_tokenizer,
                                   trainable=True,
                                   mask_zero=True)

#generate train/test split
source_train, source_val, _, _ = train_test_split(source_corpus, labels, test_size=0.01, random_state=777)
target_train, target_val, labels_train, labels_val = train_test_split(target_corpus, labels, test_size=0.01, random_state=777)

data_set_train_test = {
    'source_train':source_train,
    'source_val':source_val,
    'target_train':target_train,
    'target_val':target_val,
    'labels_train':labels_train,
    'labels_val':labels_val
}

with open('models/train_test_split_extended.p', 'wb') as handle:
    pickle.dump(data_set_train_test, handle)
#
# target_train_onehot = np.zeros((len(target_train), target_max_sequence, len(target_vocab)+1))
# for seq_id, sequence in enumerate(target_train):
#     for item_id, item in enumerate(sequence):
#         if item_id > 0:
#             target_train_onehot[seq_id][item_id-1][int(item)]=1
#
# target_val_onehot = np.zeros((len(target_val), target_max_sequence, len(target_vocab)+1))
# for seq_id, sequence in enumerate(target_val):
#     for item_id, item in enumerate(sequence):
#         if item_id > 0:
#             target_val_onehot[seq_id][item_id-1][int(item)]=1
#
# print(target_train_onehot.shape, target_val_onehot.shape)
# print("Prepared test and validation data set and labels: {}, {}, {}, {}, {}, {}".format(
#     len(source_train),
#     len(target_train),
#     len(source_val),
#     len(target_val),
#     len(labels_train),
#     len(labels_val)
#     )
# )
