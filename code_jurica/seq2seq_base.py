import tqdm
import numpy as np
import os
from typing import List, Callable, Dict, Iterable

from keras import Model
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder

from util import report_to_df


def run_pipeline_prediction(sentences: List[str], decode_seq_fnc: Callable, icd10_model: Model,
                            icd10_encoder: LabelEncoder, gold_labels: List,
                            source_tokenizer: Tokenizer, max_source_length: int,
                            icd10_tokenizer: Tokenizer, max_icd10_length: int,
                            target_index_to_word_dict: Dict, gold_target_indexes,
                            log_dir: str):

    os.makedirs(log_dir, exist_ok=True)

    y_true = []
    y_pred = []

    source_val = source_tokenizer.texts_to_sequences(sentences)
    source_val = pad_sequences(source_val, maxlen=max_source_length, padding='post')

    pred_texts = []
    pred_ids = []

    gold_texts = []
    gold_ids = []

    for seq_index in tqdm.tqdm(range(len(source_val)), total=len(source_val)):
        # for seq_index in range(10):
        inp_seq = source_val[seq_index:seq_index + 1]
        translated_sent, translated_index = decode_seq_fnc(inp_seq)

        # PREDICT ICD10
        source_word_sequence = icd10_tokenizer.texts_to_sequences([" ".join(translated_sent)])
        word_sequence = pad_sequences(source_word_sequence, maxlen=max_icd10_length, padding='post')
        icd10_code_index = icd10_model.predict(word_sequence)
        # print(icd10_code_index, type(icd10_code_index))
        max_val_index = np.argmax(icd10_code_index, axis=1)[0]

        # print(max_val_index)
        icd10_label = icd10_encoder.inverse_transform(max_val_index)

        y_true.append(gold_labels[seq_index])
        y_pred.append(icd10_label)

        # Debugging prediction
        pred_text = " ".join(translated_sent)
        pred_texts.append(pred_text)

        pred_indexes = " ".join([str(i) for i in translated_index])
        pred_ids.append(pred_indexes)

        gold_indexes = np.trim_zeros(gold_target_indexes[seq_index], 'b')[1:-1]
        gold_ids.append(" ".join([str(i) for i in gold_indexes]))

        gold_text = " ".join([target_index_to_word_dict[x] for x in gold_indexes if x in target_index_to_word_dict])
        gold_texts.append(gold_text)

        print('Target indexes:', gold_indexes)
        print('Decoded indexes:', pred_indexes)

        print('Target text:', gold_text)
        print('Decoded text:', pred_text)

        print('Target ICD-10:', gold_labels[seq_index])
        print('Predict ICD-10:', icd10_label)

        print('----------------------------------------------')


    # Write classication report
    report = classification_report(y_true, y_pred)
    report_df = report_to_df(report)
    report_file = os.path.join(log_dir, 'classification_report.csv')
    report_df.to_csv(report_file)
    print(report_df)

    # Save prediction debugging information
    save_list_to_file(pred_texts, os.path.join(log_dir, 'pred_texts.txt'))
    save_list_to_file(pred_ids, os.path.join(log_dir, 'pred_ids.txt'))

    save_list_to_file(gold_texts, os.path.join(log_dir, 'gold_texts.txt'))
    save_list_to_file(gold_ids, os.path.join(log_dir, 'gold_ids.txt'))


def save_list_to_file(values: Iterable, output_file: str):
    with open(output_file, "w", encoding="utf-8") as output_writer:
        output_content = ["%s\n" % value for value in values]
        output_writer.writelines(output_content)
        output_writer.close()


