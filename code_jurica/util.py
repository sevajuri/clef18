#!/usr/bin/python
# -*- coding: utf-8 -*-
from nltk import TreebankWordTokenizer
from sklearn.base import BaseEstimator, TransformerMixin
import re
from unidecode import unidecode
from _config import *
import string
import numpy as np
import pandas as pd
from tqdm import tqdm
import pickle
from fastText import load_model
import math
import datetime
from io import StringIO
import keras
from keras.preprocessing.sequence import pad_sequences
from collections import Counter
import random
import os

#REPRODUCIBLE
np.random.seed(42)
random.seed(12345)
os.environ['PYTHONHASHSEED'] = '0'
now = datetime.datetime.now()
date_label=now.strftime("%Y_%m_%d")
lower_ahead = "(?=[a-z0-9])"
nonword_behind = "(?<=\W)"
model_FR = load_model('data/embeddings/cc.fr.300.bin')
model_HU = load_model('data/embeddings/cc.hu.300.bin')
model_IT = load_model('data/embeddings/cc.it.300.bin')

regex_concept_dict = [
    # biomedical
    (re.compile("\w+inib$"), "_chemical_"),
    (re.compile("\w+[ui]mab$"), "_chemical_"),
    (re.compile("->|-->"), "_replacement_"),
    # (re.compile("^(PFS|pfs)$"), "progression-free survival"),

    # number-related concepts
    (re.compile("^[Pp]([=<>≤≥]|</?=|>/?=)\d"), "_p_val_"),
    (re.compile("^((\d+-)?year-old|y\.?o\.?)$"), "_age_"),
    (re.compile("^~?-?\d*[·.]?\d+--?\d*[·.]?\d+$"), "_range_"),
    (re.compile("[a-zA-Z]?(~?[=<>≤≥]|</?=|>/?=)\d?|^(lt|gt|geq|leq)$"), "_ineq_"),
    (re.compile("^~?\d+-fold$"), "_n_fold_"),
    (re.compile("^~?\d+/\d+$|^\d+:\d+$"), "_ratio_"),
    (re.compile("^~?-?\d*[·.]?\d*%$"), "_percent_"),
    (re.compile("^~?\d*(("
                "(kg|\d+g|mg|ug|ng)|"
                "(\d+m|cm|mm|um|nm)|"
                "(\d+l|ml|cl|ul|mol|mmol|nmol|mumol|mo))/?)+$"), "_unit_"),
    # abbreviation starting with letters and containing nums
    (re.compile("^[Rr][Ss]\d+$|"
                "^[Rr]\d+[A-Za-z]$"), "_mutation_"),
    (re.compile("^[a-zA-Z]\w*-?\w*\d+\w*$"), "_abbrev_"),
    # time
    (re.compile("^([jJ]an\.(uary)?|[fF]eb\.(ruary)?|[mM]ar\.(ch)?|"
                "[Aa]pr\.(il)?|[Mm]ay\.|[jJ]un\.(e)?|"
                "[jJ]ul\.(y)?|[aA]ug\.(ust)?|[sS]ep\.(tember)?|"
                "[oO]ct\.(ober)?|[nN]ov\.(ember)?|[dD]ec\.(ember)?)$"), "_month_"),
    (re.compile("^(19|20)\d\d$"), "_year_"),
    # numbers
    (re.compile("^(([Zz]ero(th)?|[Oo]ne|[Tt]wo|[Tt]hree|[Ff]our(th)?|"
                "[Ff]i(ve|fth)|[Ss]ix(th)?|[Ss]even(th)?|[Ee]ight(th)?|"
                "[Nn]in(e|th)|[Tt]en(th)?|[Ee]leven(th)?|"
                "[Tt]went(y|ieth)?|[Tt]hirt(y|ieth)?|[Ff]ort(y|ieth)?|[Ff]ift(y|ieth)?|"
                "[Ss]ixt(y|ieth)?|[Ss]event(y|ieth)?|[Ee]ight(y|ieth)?|[Nn]inet(y|ieth)?|"
                "[Mm]illion(th)?|[Bb]illion(th)?|"
                "[Tt]welv(e|th)|[Hh]undred(th)?|[Tt]housand(th)?|"
                "[Ff]irst|[Ss]econd|[Tt]hird|\d*1st|\d*2nd|\d*3rd|\d+-?th)-?)+$"), "_num_"),
    (re.compile("^~?-?\d+(,\d\d\d)*$"), "_num_"),  # int (+ or -)
    (re.compile("^~?-?((-?\d*[·.]\d+$|^-?\d+[·.]\d*)(\+/-)?)+$"), "_num_"),  # float (+ or -)
    # misc. abbrevs
    (re.compile("^[Vv]\.?[Ss]\.?$|^[Vv]ersus$"), "vs"),
    (re.compile("^[Ii]\.?[Ee]\.?$"), "ie"),
    (re.compile("^[Ee]\.?[Gg]\.?$"), "eg"),
    (re.compile("^[Ii]\.?[Vv]\.?$"), "iv"),
    (re.compile("^[Pp]\.?[Oo]\.?$"), "po")
]

prenormalize_dict = [
    # common abbreviations
    (re.compile(nonword_behind + "[Cc]a\.\s" + lower_ahead), "ca  "),
    (re.compile(nonword_behind + "[Ee]\.[Gg]\.\s" + lower_ahead), "e g  "),
    (re.compile(nonword_behind + "[Ee][Gg]\.\s" + lower_ahead), "e g "),
    (re.compile(nonword_behind + "[Ii]\.[Ee]\.\s" + lower_ahead), "i e  "),
    (re.compile(nonword_behind + "[Ii][Ee]\.\s" + lower_ahead), "i e "),
    (re.compile(nonword_behind + "[Aa]pprox\.\s" + lower_ahead), "approx  "),
    (re.compile(nonword_behind + "[Nn]o\.\s" + lower_ahead), "no  "),
    (re.compile(nonword_behind + "[Nn]o\.\s" + "(?=\w\d)"), "no  "),  # no. followed by abbreviation (patient no. V123)
    (re.compile(nonword_behind + "[Cc]onf\.\s" + lower_ahead), "conf  "),
    # scientific writing
    (re.compile(nonword_behind + "et al\.\s" + lower_ahead), "et al  "),
    (re.compile(nonword_behind + "[Rr]ef\.\s" + lower_ahead), "ref  "),
    (re.compile(nonword_behind + "[Ff]ig\.\s" + lower_ahead), "fig  "),
    # medical
    (re.compile(nonword_behind + "y\.o\.\s" + lower_ahead), "y o  "),
    (re.compile(nonword_behind + "yo\.\s" + lower_ahead), "y o "),
    (re.compile(nonword_behind + "[Pp]\.o\.\s" + lower_ahead), "p o  "),
    (re.compile(nonword_behind + "[Ii]\.v\.\s" + lower_ahead), "i v  "),
    (re.compile(nonword_behind + "[Bb]\.i\.\d\.\s" + lower_ahead), "b i d  "),
    (re.compile(nonword_behind + "[Tt]\.i\.\d\.\s" + lower_ahead), "t i d  "),
    (re.compile(nonword_behind + "[Qq]\.i\.\d\.\s" + lower_ahead), "q i d  "),
    (re.compile(nonword_behind + "J\.\s" + "(?=(Cell|Bio|Med))"), "J  "),  # journal
    # bracket complications
    # (re.compile("\.\)\."), " )."),
    # (re.compile("\.\s\)\."), "  )."),
    # multiple dots
    # (re.compile("(\.+\s*\.+)+"), "."),
    # # Typos: missing space after dot; only add space if there are at least two letters before and behind
    # (re.compile("(?<=[A-Za-z]{2})" + "\." + "(?=[A-Z][a-z])"), ". "),
    # whitespace
    (re.compile("\s"), " "),
]

def report_to_df(report):
    report = re.sub(r" +", " ", report).replace("avg / total", "avg/total").replace("\n ", "\n")
    report_df = pd.read_csv(StringIO("Classes" + report), sep=' ', index_col=0)
    return(report_df)

def map_regex_concepts(token):
    """replaces abbreviations matching simple REs, e.g. for numbers, percentages, gene names, by class tokens"""
    for regex, repl in regex_concept_dict:
        if regex.findall(token):
            return repl
    return token

def prenormalize(text):
    """normalize common abbreviations and symbols known to mess with sentence boundary disambiguation"""
    for regex, repl in prenormalize_dict:
        text = regex.sub(repl, text)
    return text

def flatten(l):
    """flatten 2-dimensional sequence to one-dimensional"""
    return [item for sublist in l for item in sublist]

def embedding_matrix(vocabulary, EMBEDDING_DIM=900):
    embedding_matrix = np.zeros((len(vocabulary)+1, EMBEDDING_DIM))
    # print(len(vocabulary), embedding_matrix.shape)
    for word, i in vocabulary.items():
        embedding_vector_FR = model_FR.get_word_vector(word)
        embedding_vector_HU = model_HU.get_word_vector(word)
        embedding_vector_IT = model_IT.get_word_vector(word)
        embedding_matrix[i] = np.concatenate([embedding_vector_FR, embedding_vector_HU, embedding_vector_IT])
    return embedding_matrix

class TextNormalizer(BaseEstimator, TransformerMixin):
    """replaces all non-ASCII characters by approximations, all numbers by 1"""

    def __init__(self):
        return

    def fit(self, X=None, y=None):
        return self

    @staticmethod
    def transform(X):
        return np.array([unidecode(x) for x in X])

class TokenizePreprocessor(BaseEstimator, TransformerMixin):
    def __init__(self, rules=True):
        self.punct = set(string.punctuation).difference(set('%='))
        self.rules = rules
        self.splitters = re.compile("[-/.,|<>]")
        self.tokenizer = TreebankWordTokenizer()

    def fit(self, X=None, y=None):
        return self

    @staticmethod
    def inverse_transform(X):
        return [", ".join(doc) for doc in X]

    def transform(self, X):
        return [self.token_representation(sentence) for sentence in X]

    def token_representation(self, sentence):
        return list(self.tokenize(sentence))

    def tokenize(self, sentence):
        """break sentence into pos-tagged tokens; normalize and split on hyphens"""

        # extremely short sentences shall be ignored by next steps

        # if len(sentence) < MIN_LEN:
        #     yield "_empty_sentence_"
        # else:

        for token in self.tokenizer.tokenize(sentence):
            # Apply preprocessing to the token
            token_nrm = self.normalize_token(token)
            subtokens = [self.normalize_token(t) for t in self.splitters.split(token_nrm)]

            for subtoken in subtokens:
                # If punctuation, ignore token and continue
                if all(char in self.punct for char in token):
                    continue
                yield subtoken

    def normalize_token(self, token):
        # Apply preprocessing to the token
        token = token.lower().strip().strip('*').strip('.')
        if self.rules:
            token = map_regex_concepts(token)
        return token

class prepareData():

    def __init__(self):
        # self.tokenizer = TokenizePreprocessor()
        # self.vocabulary=None
        pass

    def prepareData(self, dataset):
        data = None
        errors = None

        try:
            data = pickle.load(open('data/preprocesed/Train_{}.p'.format(dataset),'rb'))
            errors = pickle.load(open('data/preprocesed/Errors_{}.p'.format(dataset),'rb'))

        except FileNotFoundError:
            file_encoding = TRAINING[dataset]['Encoding']
            split_multi_code = TRAINING[dataset]['SplitMultiCode']

            cc_files = TRAINING[dataset]['CC']
            cc_data = [pd.read_csv(cc_file, sep=';', encoding=file_encoding, skipinitialspace=True) for cc_file in cc_files]
            cc = pd.concat(cc_data)

            cb_files = TRAINING[dataset]['CB']
            cb_data = [pd.read_csv(cb_file, sep=';', encoding=file_encoding, skipinitialspace=True) for cb_file in cb_files]
            cb = pd.concat(cb_data)

            data = []
            errors= []

            for index, row in tqdm(cb.iterrows(), ascii=True, desc='Preparing {}'.format(dataset)):

                try:
                    text = cc[(cc.DocID == row.DocID) & (cc.LineID == row.LineID) & (cc.YearCoded == row.YearCoded)]
                    num_icd10_codes = len(text)
                    # print(text.StandardText.values[0], text.ICD10.values[0])

                    appended_data = False
                    if split_multi_code and num_icd10_codes > 1:
                        parts = row.RawText.lower().split(",")
                        if len(parts) == num_icd10_codes:
                            for i in range(num_icd10_codes):
                                data.append([parts[i], text.StandardText.values[i], text.ICD10.values[i]])

                            appended_data = True

                    if not appended_data:
                        data.append([
                            row.RawText.lower(),
                            text.StandardText.values[0].lower(),
                            text.ICD10.values[0]
                        ])

                except Exception as e:
                    # print(e)
                    # print(row.DocID, row.LineID)
                    errors.append([dataset, row.DocID, row.LineID])

            output_folder = "data/preprocesed/"
            os.makedirs(output_folder, exist_ok=True)

            pickle.dump(data, open('data/preprocesed/Train_{}.p'.format(dataset),'wb'))
            pickle.dump(errors, open('data/preprocesed/Errors_{}.p'.format(dataset),'wb'))

        random.shuffle(data)
        return data, errors


    def prepareDictionaries(self, unbalanced=False, oversampled=False):

        preparedDictionary = []

        # try:
        #     preparedDictionary = pickle.load(open(DICT_PREPROCESED, 'rb'))
        #
        # except FileNotFoundError:
        dicts = [
            DICT_FR,
            DICT_HU,
            DICT_IT
        ]

        for item in dicts:
            df = pd.read_csv(item, sep=';', dtype=str, encoding = "utf8")
            for index, row in df.iterrows():
                try:
                    text = ' '.join([row['DiagnosisText'], row['Standardized']])
                except (KeyError, TypeError):
                    text = row['DiagnosisText']

                label = str(row['Icd1']).strip().upper()[:4]

                if not isinstance(text, float):
                    preparedDictionary.append([text.lower(), label])
                else:
                    if not math.isnan(text):
                        preparedDictionary.append([ text.lower(), label ])

        if unbalanced:
            for k, v in TRAINING.items():
                df = pd.read_csv(v['CC'][0], sep=';', dtype=str, encoding="utf8")
                for index, row in df.iterrows():

                    label = str(row['ICD10']).strip().upper()[:4]
                    text = row['StandardText']

                    if not isinstance(text, float):
                        preparedDictionary.append([text.lower().strip(), label])
                    else:
                        if not math.isnan(text):
                            preparedDictionary.append([text.lower().strip(), label])

        if oversampled:
            tmp = [x[1] for x in preparedDictionary]
            labels_c = Counter(tmp)
            labels_tmp = {k: v for k, v in labels_c.items() if v < 3}
            undersampled = [x for x in preparedDictionary if x[1] in labels_tmp] * 3
            preparedDictionary += undersampled

            # pickle.dump(preparedDictionary, open(DICT_PREPROCESED, 'wb'))
        random.shuffle(preparedDictionary)
        corpus = [item[0] for item in preparedDictionary]
        labels = [item[1] for item in preparedDictionary]
        #print(Counterlabels)
        return corpus, labels

class KerasBatchGenerator(keras.utils.Sequence):

    def __init__(self, batch_size, source_corpus, source_maxlen, source_tokenizer, target_corpus, target_maxlen, target_tokenizer):

        self.batch_size = batch_size

        self.source_corpus = source_corpus
        self.target_corpus = target_corpus

        self.source_maxlen = source_maxlen
        self.target_maxlen = target_maxlen

        self.source_tokenizer = source_tokenizer
        self.target_tokenizer = target_tokenizer

        # this will track the progress of the batches sequentially through the
        # data set - once the data reaches the end of the data set it will reset
        # back to zero
        self.current_idx = 0


    def generate_data(self):
        while True:
            if self.current_idx * self.batch_size >= len(self.source_corpus):
                # reset the index back to the start of the data set
                self.current_idx = 0

            batch_source = self.source_corpus[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_source = self.source_tokenizer.texts_to_sequences(batch_source)
            batch_source = pad_sequences(batch_source, maxlen=self.source_maxlen, padding='post')

            batch_target = self.target_corpus[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_target = self.target_tokenizer.texts_to_sequences(batch_target)

            # target_train_onehot = np.zeros((self.batch_size, self.target_maxlen, len(self.target_tokenizer.word_index) + 1))
            target_train_onehot = np.zeros((len(batch_source), self.target_maxlen, len(self.target_tokenizer.word_index) + 1))
            for seq_id, sequence in enumerate(batch_target):
                for item_id, item in enumerate(sequence):
                    if item_id > 0:
                        target_train_onehot[seq_id][item_id - 1][int(item)] = 1

            batch_target = pad_sequences(batch_target, maxlen=self.target_maxlen, padding='post')
            self.current_idx += 1
            yield [batch_source, batch_target], target_train_onehot

class KerasBatchGeneratorClassification(keras.utils.Sequence):

    def __init__(self, batch_size, data, labels, timesteps, tokenizer, labeler):

        self.batch_size = batch_size
        self.data = data
        self.labels = labels
        self.timesteps = timesteps
        self.tokenizer = tokenizer
        self.labeler = labeler
        # this will track the progress of the batches sequentially through the
        # data set - once the data reaches the end of the data set it will reset
        # back to zero
        self.current_idx = 0


    def generate_data(self):
        while True:
            if self.current_idx * self.batch_size >= len(self.source_corpus):
                # reset the index back to the start of the data set
                self.current_idx = 0

            batch_source = self.data[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_source = self.tokenizer.texts_to_sequences(batch_source)
            batch_source = pad_sequences(batch_source, maxlen=self.timesteps, padding='post')

            batch_labels = self.labels[self.current_idx * self.batch_size:(self.current_idx + 1) * self.batch_size]
            batch_labels = self.labeler.transform(batch_labels)

            self.current_idx += 1
            yield batch_source, batch_labels
            
# dataLoader=prepareData()
# corpus, labels =dataLoader.prepareDictionaries(unbalanced=False, oversampled=False)
# corpus, labels =dataLoader.prepareDictionaries(unbalanced=True, oversampled=False)
