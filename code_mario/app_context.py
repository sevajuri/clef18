import os
from common_util import LogUtil


class AppContext(object):

    default_context = None

    def __init__(self, default_log_file: str, log_dir: str, output_dir: str):
        self.default_log_file = default_log_file
        self.log_dir = log_dir
        self.output_dir = output_dir

    @staticmethod
    def default():
        return AppContext.default_context

    @staticmethod
    def initialize_by_app_name(app_name: str):
        log_dir = LogUtil.create_timestamped_log_dir(app_name)
        log_file = os.path.join(log_dir, "{}-application.log".format(app_name))

        AppContext.default_context = AppContext(log_file, log_dir, log_dir)
        return AppContext.default_context

    @staticmethod
    def initialize(default_log_file: str, log_dir: str, output_dir: str):
        AppContext.default_context = AppContext(default_log_file, log_dir, output_dir)
        return AppContext.default_context
