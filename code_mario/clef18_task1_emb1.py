import concurrent

from gensim.models import FastText

from init import *
from concurrent.futures import ThreadPoolExecutor

from clef18_task1_base import ICD10LabelEncoders, Clef18Task1Base, EvaluationConfiguration, NegativeSampling
from clef18_task1_emb2 import EvaluationResult

import argparse
import numpy as np
import pandas as pd
import keras as k
import os

from keras import Input, Model
from keras.layers import Bidirectional, Dense, Dot, LSTM, Embedding
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from pandas import DataFrame
from sklearn.metrics import f1_score, accuracy_score
from sklearn.pipeline import Pipeline
from tqdm import tqdm
from typing import Tuple, Dict, List, Callable

from app_context import AppContext
from clef18_task1_data import Clef18Task1Data
from ft_embeddings import FastTextEmbeddings, FastTextModel, SingleLanguageFastTextModel
from preprocessing import DataPreparationUtil as pdu
from keras_extension import KerasUtil as ku


class Emb1Configuration(object):

    def __init__(self, train_cert_df: DataFrame, val_cert_df: DataFrame, test_cert_df: DataFrame, dict_df: DataFrame,
                 max_cert_length: int, max_dict_length: int, ft_embedding_size: int, label_column: str,
                 label_encoders: ICD10LabelEncoders, keras_tokenizer: Tokenizer):
        self.train_cert_df = train_cert_df
        self.val_cert_df = val_cert_df
        self.test_cert_df = test_cert_df
        self.dict_df = dict_df
        self.max_cert_length = max_cert_length
        self.max_dict_length = max_dict_length
        self.ft_embedding_size = ft_embedding_size
        self.label_column = label_column
        self.label_encoders = label_encoders
        self.keras_tokenizer = keras_tokenizer

class Clef18Task1Emb1(Clef18Task1Base):

    def __init__(self):
        Clef18Task1Base.__init__(self)

    def build_embedding_model(self, word_index: Dict, ft_model: FastTextModel, max_cert_length: int, max_dict_length: int):
        # TODO: Make hyper-parameter configurable!
        embedding_matrix = np.zeros((len(word_index) + 1, ft_model.vector_size()))
        for word, i in word_index.items():
            try:
                language = word[0:2]
                token = word[2:]
                embedding_vector = ft_model.lookup(token, language)
                if embedding_vector is not None:
                    # words not found in embedding index will be all-zeros.
                    embedding_matrix[i] = embedding_vector
            except KeyError:
                self.logger.error("Can't create embedding for '%s'", word)

        embedding = Embedding(len(word_index)+1, ft_model.vector_size(), weights=[embedding_matrix], mask_zero=True)

        # Model 1: Learn a representation of a line originating from a death certificate
        input_certificate_line = Input((max_cert_length, ))
        cert_embeddings = embedding(input_certificate_line)
        certificate_rnn = Bidirectional(LSTM(200), name="cert_rnn")(cert_embeddings)

        # Model 2: Learn a representation of a line in the ICD-10 dictionary (~ DiagnosisText)
        input_dictionary_line = Input((max_dict_length, ))
        dictionary_embeddings = embedding(input_dictionary_line)
        dictionary_rnn = Bidirectional(LSTM(200), name="dict_rnn")(dictionary_embeddings)

        # Calculate similarity between both representations
        dot_product = Dot(axes=1, normalize=True)([certificate_rnn, dictionary_rnn])

        output = Dense(1, activation='sigmoid')(dot_product)

        # Create the primary training model
        model = Model(inputs=[input_certificate_line, input_dictionary_line], outputs=output, name="ICD10-Embedding-Model1")
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=["accuracy"])

        return model

    def train_embedding_model(self, config: Emb1Configuration, ft_model: FastTextModel, max_pos_samples: int,
                              neg_sampling_strategy: Callable, epochs: int, batch_size: int) -> Model:
        self.logger.info("Start building embedding model")
        model = self.build_embedding_model(config.keras_tokenizer.word_index, ft_model, config.max_cert_length, config.max_dict_length)
        model.summary(print_fn=self.logger.info)

        self.logger.info("Start building training pairs")
        train_pair_data = self.build_pairs(config.train_cert_df, config.dict_df, max_pos_samples, neg_sampling_strategy)
        self.logger.info("Label distribution:\n%s", train_pair_data["Label"].value_counts())

        cert_inputs = pad_sequences(train_pair_data["Cert_input"].values, maxlen=config.max_cert_length, padding="post")
        dict_inputs = pad_sequences(train_pair_data["Dict_input"].values, maxlen=config.max_dict_length, padding="post")
        labels = train_pair_data["Label"].values

        self.logger.info("Start training of embedding model")
        best_model_file = os.path.join(AppContext.default().output_dir, "embedding_model_best.h5")

        if config.val_cert_df is not None and len(config.test_cert_df) > 0:
            self.logger.info("Start creation of validation pairs")
            val_pair_data = self.build_pairs(config.val_cert_df, config.dict_df, max_pos_samples, neg_sampling_strategy)

            val_cert_inputs = pad_sequences(val_pair_data["Cert_input"].values, maxlen=config.max_cert_length, padding="post")
            val_dict_inputs = pad_sequences(val_pair_data["Dict_input"].values, maxlen=config.max_dict_length, padding="post")
            val_gold_labels = val_pair_data["Label"].values

            val_data = ([val_cert_inputs, val_dict_inputs], val_gold_labels)
            model.fit([cert_inputs, dict_inputs], labels, epochs=epochs, batch_size=batch_size, validation_data=val_data,
                      callbacks=[ku.best_model_checkpointing_by_file_path(best_model_file, "val_loss")])
        else:
            model.fit([cert_inputs, dict_inputs], labels, epochs=epochs, batch_size=batch_size,
                        callbacks=[ku.best_model_checkpointing_by_file_path(best_model_file)])

        model_file = os.path.join(AppContext.default().output_dir, "embedding_model_last.h5")
        self.logger.info("Saving last model to %s", model_file)
        model.save(model_file)

        self.logger.info("Reloading best embedding model from %s", best_model_file)
        model = k.models.load_model(best_model_file)

        ## ----------------------------------------------------------------------------------------------------------

        if config.val_cert_df is not None and len(config.val_cert_df) > 0:
            self.logger.info("Start evaluation of embedding model!")

            self.logger.info("Start creation of test pairs")
            test_pair_data = self.build_pairs(config.test_cert_df, config.dict_df, max_pos_samples, neg_sampling_strategy)

            test_cert_inputs = pad_sequences(test_pair_data["Cert_input"].values, maxlen=config.max_cert_length, padding="post")
            test_dict_inputs = pad_sequences(test_pair_data["Dict_input"].values, maxlen=config.max_dict_length, padding="post")
            test_gold_labels = test_pair_data["Label"].values

            self.logger.info("Start prediction of test labels")
            pred_labels = model.predict([test_cert_inputs, test_dict_inputs], verbose=1)
            pred_labels = (pred_labels > 0.5).astype(float)

            f1_value = f1_score(test_gold_labels, pred_labels)
            acc_value = accuracy_score(test_gold_labels, pred_labels)

            self.logger.info("Result: f1_score= %s | acc_score= %s", f1_value, acc_value)

        return model

    # ---------------------------------------------------------------------------------------------------------------------------------------

    def train_and_evaluate_classifiers(self, emb_model: Model, config: Emb1Configuration, target_labels: List) -> List[EvaluationResult]:
        self.logger.info("Start training and evaluation of classifier models")

        cert_input = emb_model.inputs[0]
        cert_rnn = Model(inputs=cert_input, outputs=emb_model.get_layer("cert_rnn").output, name="Cert-RNN-Model")

        self.logger.info("Building dictionary embeddings")
        dict_input = emb_model.inputs[1]
        #dict_rnn = Model(inputs=dict_input, outputs=emb_model.get_layer("dict_rnn").output, name="Dict-RNN-Model")
        dict_inputs = pad_sequences(config.dict_df["Token_ids"].values, maxlen=config.max_cert_length, padding="post")
        dict_embeddings = cert_rnn.predict(dict_inputs, verbose=1, batch_size=1)

        self.logger.info("Building train certificate embeddings")
        train_cert_inputs = pad_sequences(config.train_cert_df["Token_ids"].values, maxlen=config.max_cert_length, padding="post")
        train_cert_embeddings = cert_rnn.predict(train_cert_inputs, verbose=1)
        self.logger.info("cert train input shape: %s", train_cert_embeddings.shape)

        self.logger.info("Building val certificate embeddings")
        val_inputs = pad_sequences(config.val_cert_df["Token_ids"].values, maxlen=config.max_cert_length, padding="post")
        val_embeddings = cert_rnn.predict(val_inputs, verbose=1)
        self.logger.info("cert val input shape: %s", val_embeddings.shape)

        self.logger.info("Building test certificate embeddings")
        test_inputs = pad_sequences(config.test_cert_df["Token_ids"].values, maxlen=config.max_cert_length, padding="post")
        test_embeddings = cert_rnn.predict(test_inputs, verbose=1)
        self.logger.info("cert test input shape: %s", test_embeddings.shape)

        target_label_configs = self.get_label_configuration(target_labels, config.label_encoders)
        target_label_columns = [label_column for _, label_column, _ in target_label_configs]

        train_labels = pd.concat([config.dict_df[target_label_columns], config.train_cert_df[target_label_columns]])
        train_data = np.append(dict_embeddings, train_cert_embeddings, axis=0)

        input_dim = cert_rnn.output.shape[1].value

        eval_configuration = EvaluationConfiguration(target_labels, config.label_encoders, input_dim, train_data, train_labels,
                                                     val_embeddings, config.val_cert_df, test_embeddings, config.test_cert_df)
        return self.run_evaluation(eval_configuration)

    # ---------------------------------------------------------------------------------------------------------------------------------------

    def predict(self, emb_model: Model, classifier, conf: Emb1Configuration, test_df: DataFrame) -> DataFrame:
        self.logger.info("Start preprocessing test data")
        preprocessing_pipeline = Pipeline([
            ("LowercaseText", pdu.to_lowercase("RawText")),
            ("TokenizeText", pdu.keras_sequencing("RawText", "Word_ids", conf.keras_tokenizer, False))
        ])

        #test_df = Clef18Task1Data().read_it_train_certificates()

        test_df = preprocessing_pipeline.fit_transform(test_df)
        test_emb_inputs = pad_sequences(test_df["Word_ids"].values, maxlen=conf.max_cert_length, padding="post")
        self.logger.info("Finished preprocessing of test data (shape: %s)", test_emb_inputs.shape)

        self.logger.info("Start generation of embeddings")
        cert_input = emb_model.inputs[0]
        cert_rnn = Model(inputs=cert_input, outputs=emb_model.get_layer("cert_rnn").output, name="Cert-RNN-Model")
        test_cl_inputs = cert_rnn.predict(test_emb_inputs, 16, verbose=1)

        predictions = classifier.predict(test_cl_inputs)
        predicted_labels = conf.label_encoders.code_encoder.inverse_transform(predictions)

        result = DataFrame(columns=["DocID", "YearCoded", "LineID", "Rank", "StandardText", "ICD10", "IntervalText"])
        for i, (id, row) in tqdm(enumerate(test_df.iterrows()), desc="build-result", total=len(test_df)):
            result = result.append({"DocID": id[1], "YearCoded" : id[0], "LineID" : id[2], "Rank": "",
                                    "StandardText": "", "ICD10" : predicted_labels[i], "IntervalText": ""}, ignore_index=True)

        output_file = os.path.join(AppContext.default().output_dir, "prediction_train.csv")
        result.to_csv(output_file, sep=";", index=False)
        return result

    # ---------------------------------------------------------------------------------------------------------------------------------------

    def prepare_data_set(self, cert_df: DataFrame, dict_df: DataFrame, ft_model: FastTextModel, train_ratio: float, val_ratio: float,
                         strat_column: str, stratified_splits: bool=False) -> Emb1Configuration:

        self.logger.info("Splitting certificate lines into train and evaluation data set")
        train_cert_df, evaluation_cert_df = self.split_train_test(cert_df, train_ratio, stratified_splits, strat_column)
        self.logger.info("Finished splitting: train=%s instances, evaluation=%s instances", len(train_cert_df), len(evaluation_cert_df))

        self.logger.info("Splitting evaluation data set into validation and test set")
        val_cert_df, test_cert_df = self.split_train_test(evaluation_cert_df, val_ratio, stratified_splits, strat_column)

        label_encoders = self.prepare_label_encoders(dict_df, cert_df)
        keras_tokenizer = Tokenizer(oov_token="<UNK>")

        self.logger.info("Start preparation of training cert data (%s instances)", len(train_cert_df))
        train_cert_df, max_cert_length = self.prepare_certificate_df(train_cert_df, "train", label_encoders, keras_tokenizer)

        self.logger.info("Start preparation of validation cert data (%s instances)", len(val_cert_df))
        val_cert_df, _ = self.prepare_certificate_df(val_cert_df, "validation", label_encoders, keras_tokenizer)

        self.logger.info("Start preparation of test cert data (%s instances)", len(test_cert_df))
        test_cert_df, _ = self.prepare_certificate_df(test_cert_df, "test", label_encoders, keras_tokenizer)

        self.logger.info("Start preparation of dictionary data (%s instances)", len(dict_df))
        dict_df, max_dict_length = self.prepare_dictionary_df(dict_df, "train", label_encoders, keras_tokenizer)

        return Emb1Configuration(train_cert_df, val_cert_df, test_cert_df, dict_df, max_cert_length, max_dict_length,
                                 ft_model.vector_size(), strat_column, label_encoders, keras_tokenizer)

    def prepare_certificate_df(self, certificate_df: DataFrame, mode: str, icd10_encoders: ICD10LabelEncoders,
                               keras_tokenizer: Tokenizer) -> Tuple[DataFrame, int]:
        certificate_pipeline = Pipeline([
            ("Extract-ICD10-chapter", pdu.extract_icd10_chapter("ICD10", "ICD10_chapter")),
            ("Encode-ICD10-chapter", pdu.encode_labels("ICD10_chapter", "ICD10_chapter_encoded",
                                                       icd10_encoders.chapter_encoder, False)),

            ("Extract-ICD10-section", pdu.extract_icd10_section("ICD10", "ICD10_section")),
            ("Encode-ICD10-section", pdu.encode_labels("ICD10_section", "ICD10_section_encoded",
                                                       icd10_encoders.section_encoder, False)),

            ("Extract-ICD10-subsection", pdu.extract_icd10_subsection("ICD10", "ICD10_subsection")),
            ("Encode-ICD10-subsection", pdu.encode_labels("ICD10_subsection", "ICD10_subsection_encoded",
                                                       icd10_encoders.subsection_encoder, False)),

            ("Clean-ICD10-code", pdu.strip("ICD10")),
            ("Encode-ICD10-code", pdu.encode_labels("ICD10", "ICD10_encoded",
                                                     icd10_encoders.code_encoder, False)),

            ("LowercaseText", pdu.to_lowercase("RawText")),
            ("TokenizeText", pdu.keras_sequencing("RawText", "Token_ids", keras_tokenizer, (mode == "train")))
        ])

        cert_data_prepared = certificate_pipeline.fit_transform(certificate_df)

        if mode == "train":
            max_length = max([len(array) for array in cert_data_prepared["Token_ids"].values])
        else:
            max_length = None

        return cert_data_prepared, max_length

    def prepare_dictionary_df(self, dictionary_df: DataFrame, mode: str, icd10_encoders: ICD10LabelEncoders,
                              keras_tokenizer: Tokenizer) -> Tuple[DataFrame, int]:
        dictionary_pipeline = Pipeline([
            ("Extract-ICD10-chapter", pdu.extract_icd10_chapter("ICD10", "ICD10_chapter")),
            ("Encode-ICD10-chapter", pdu.encode_labels("ICD10_chapter", "ICD10_chapter_encoded",
                                                       icd10_encoders.chapter_encoder, False)),

            ("Extract-ICD10-section", pdu.extract_icd10_section("ICD10", "ICD10_section")),
            ("Encode-ICD10-section", pdu.encode_labels("ICD10_section", "ICD10_section_encoded",
                                                       icd10_encoders.section_encoder, False)),

            ("Extract-ICD10-subsection", pdu.extract_icd10_subsection("ICD10", "ICD10_subsection")),
            ("Encode-ICD10-subsection", pdu.encode_labels("ICD10_subsection", "ICD10_subsection_encoded",
                                                       icd10_encoders.subsection_encoder, False)),

            ("Clean-ICD10-code", pdu.strip("ICD10")),
            ("Encode-ICD10-code", pdu.encode_labels("ICD10", "ICD10_encoded",
                                                     icd10_encoders.code_encoder, False)),

            ("CombineTexts", pdu.combine_texts(["DiagnosisText", "Standardized"], "DictText")),
            ("LowercaseText", pdu.to_lowercase("DictText")),

            ("TokenizeText", pdu.keras_sequencing("DictText", "Token_ids", keras_tokenizer, (mode == "train")))
        ])

        dict_data_prepared = dictionary_pipeline.fit_transform(dictionary_df)
        max_length = max([len(array) for array in dict_data_prepared["Token_ids"].values])

        return dict_data_prepared, max_length

    def build_pairs(self, certificate_data: DataFrame, dictionary_data: DataFrame, max_pos_samples: int, neg_sampling_strategy: Callable):
        # FIXME: Use negative sample ratio (depending on true dictionary entries), e.g. 0.5 or 1.2

        certificate_vectors = []
        dictionary_vectors = []
        labels = []

        for i, cert_row in tqdm(certificate_data.iterrows(), desc="build-pairs", total=len(certificate_data)):
            line_icd10_code = cert_row["ICD10"]

            # Build positive examples (based on training data)
            dictionary_entries = dictionary_data.query("ICD10 == '%s'" % line_icd10_code)
            if len(dictionary_entries) > 0:
                dictionary_entries = dictionary_entries.sample(min(max_pos_samples, len(dictionary_entries)))
            else:
                # Add at least one example
                certificate_vectors.append(cert_row["Token_ids"])
                dictionary_vectors.append(cert_row["Token_ids"])
                labels.append(1.0)

            #self.logger.info("Found %s entries for ICD-10 code %s", len(dictionary_entries), line_icd10_code)
            for i, dict_row in dictionary_entries.iterrows():
                certificate_vectors.append(cert_row["Token_ids"])
                dictionary_vectors.append(dict_row["Token_ids"])

                labels.append(1.0)

            # Build negative samples
            # Find illegal ICD-10 for this line
            negative_samples = neg_sampling_strategy(certificate_data, line_icd10_code)

            for i, neg_row in negative_samples.iterrows():
                certificate_vectors.append(cert_row["Token_ids"])
                dictionary_vectors.append(neg_row["Token_ids"])

                labels.append(0.0)

        data = {"Cert_input": certificate_vectors, "Dict_input": dictionary_vectors, "Label": labels}
        return pd.DataFrame(data)

    def build_pairs_para(self, certificate_data: DataFrame, dictionary_data: DataFrame, max_pos_samples: int,
                    neg_sampling_strategy: Callable, workers: int, chunk_size: int):
        # FIXME: This can be implemented more efficiently!
        # FIXME: Use negative sample ratio (depending on true dictionary entries), e.g. 0.5 or 1.2

        def _run_build_pairs(df_slice: DataFrame):
            loc_certificate_vectors = []
            loc_dictionary_vectors = []
            loc_labels = []

            for i, cert_row in df_slice.iterrows():
                line_icd10_code = cert_row["ICD10"]

                # Build positive examples (based on training data)
                dictionary_entries = dictionary_data.query("ICD10 == '%s'" % line_icd10_code)
                dictionary_entries = dictionary_entries.sample(min(max_pos_samples, len(dictionary_entries)))

                #self.logger.info("Found %s entries for ICD-10 code %s", len(dictionary_entries), line_icd10_code)
                for i, dict_row in dictionary_entries.iterrows():
                    loc_certificate_vectors.append(cert_row["Token_ids"])
                    loc_dictionary_vectors.append(dict_row["Token_ids"])

                    loc_labels.append(1.0)

                # Build negative samples
                # Find illegal ICD-10 for this line
                negative_samples = neg_sampling_strategy(certificate_data, line_icd10_code)

                for i, neg_row in negative_samples.iterrows():
                    loc_certificate_vectors.append(cert_row["Token_ids"])
                    loc_dictionary_vectors.append(neg_row["Token_ids"])

                    loc_labels.append(0.0)

            return loc_certificate_vectors, loc_dictionary_vectors, loc_labels

        certificate_vectors = []
        dictionary_vectors = []
        labels = []

        with ThreadPoolExecutor(max_workers=workers) as executor:
            futures = []
            for i in range(0, len(certificate_data), chunk_size):
                futures.append(executor.submit(_run_build_pairs, certificate_data[i:i + chunk_size]))

            compl_futures = concurrent.futures.as_completed(futures)
            for future in tqdm(compl_futures, desc="build-pairs", total=len(futures)):
                slice_result = future.result()
                certificate_vectors = certificate_vectors + slice_result[0]
                dictionary_vectors = dictionary_vectors + slice_result[1]
                labels = labels + slice_result[2]

        data = {"Cert_input": certificate_vectors, "Dict_input": dictionary_vectors, "Label": labels}
        return pd.DataFrame(data)

    def build_rnn_input(self, data: DataFrame, column: str, max_length: int, vector_size: int) -> np.ndarray:
        data_matrix = np.zeros((len(data), max_length, vector_size))

        for i, (_, row) in tqdm(enumerate(data.iterrows()), desc="build-matrices", total=len(data)):
            data_matrix[i] = row[column]

        return data_matrix


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="CLEF2018")
    subparsers = parser.add_subparsers(dest="mode")

    train_emb_parser = subparsers.add_parser("train-emb")
    train_emb_parser.add_argument("lang", help="Language to train on", choices=["it", "fr", "hu", "ac", "am"])
    train_emb_parser.add_argument("--epochs", help="Number of epochs to train", default=10, type=int)
    train_emb_parser.add_argument("--batch_size", help="Batch size during training", default=10, type=int)
    train_emb_parser.add_argument("--train_ratio", help="Ratio of samples (from the complete data set) to use for training", default=0.8, type=float)
    train_emb_parser.add_argument("--val_ratio", help="Ratio of samples (from the evaluation data set) to use for validation", default=0.5, type=float)

    train_emb_parser.add_argument("--samples", help="Number of instances to sample from the (original) training data", default=None, type=int)
    train_emb_parser.add_argument("--min_freq", help="Minimal number of instances per ICD10 code", default=10, type=int)
    train_emb_parser.add_argument("--down_sample", help="Maximal frequency of ICD10 code until start down sampling", default=None, type=int)
    train_emb_parser.add_argument("--strat_column", help="Column used to stratify the data sets", default="ICD10", type=str)
    train_emb_parser.add_argument("--strat_splits", help="Indicates whether to use stratified sampling", default=True, type=bool)
    train_emb_parser.add_argument("--strat_min_freq", help="Min frequency of an icd10 code to be an own class during stratification", default=8, type=int)
    train_emb_parser.add_argument("--target_labels", help="Target columns for the classification models", default=["icd10"], action='append')
    train_emb_parser.add_argument("--single_only", help="Indicates whether just to use the single code lines from the data", default=False, type=bool)

    train_emb_parser.add_argument("--max_pos_samples", help="Maximal number of positive samples to use", default=12, type=int)
    train_emb_parser.add_argument("--neg_sampling", help="Negative sampling strategy to use", default="ext1", choices=["def", "ext1"])
    train_emb_parser.add_argument("--num_neg_samples", help="Number of negative samples to use (default strategy)", default=75, type=int)
    train_emb_parser.add_argument("--num_neg_cha", help="Number of negative chapter samples to use (ext1 strategy)", default=10, type=int)
    train_emb_parser.add_argument("--num_neg_sec", help="Number of negative section samples to use (ext1 strategy)", default=20, type=int)
    train_emb_parser.add_argument("--num_neg_sub", help="Number of negative subsection samples to use (ext1 strategy)", default=20, type=int)
    train_emb_parser.add_argument("--num_neg_oth", help="Number of negative other samples to use (ext1 strategy)", default=10, type=int)

    eval_classifier_parser = subparsers.add_parser("eval-cl")
    eval_classifier_parser.add_argument("emb_model", help="Path to the embedding model to use")
    eval_classifier_parser.add_argument("train_conf", help="Path to the training configuration dump")
    eval_classifier_parser.add_argument("lang", help="Language to train on", choices=["it", "fr", "hu", "ac", "am"])
    eval_classifier_parser.add_argument("--target_labels", help="Target columns for the classification models", default=["icd10"], action='append')

    predict_parser = subparsers.add_parser("pred")
    predict_parser.add_argument("emb_model", help="Path to the learned embedding model to use")
    predict_parser.add_argument("cl_model", help="Path to the learned classifier model to use")
    predict_parser.add_argument("train_conf", help="Path to the training configuration dump")
    predict_parser.add_argument("lang", help="Language to train on", choices=["it", "fr", "hu"])

    args = parser.parse_args()

    AppContext.initialize_by_app_name(Clef18Task1Emb1.__name__ + "-" + args.mode)

    clef18_task1 = Clef18Task1Emb1()
    clef18_task1.save_arguments(args)

    clef_data = Clef18Task1Data()

    if args.mode == "train-emb":
        dictionary = clef_data.read_dictionary_by_id(args.lang)
        certificates = clef_data.read_train_certifcates_by_id(args.lang)

        sentences = [["cat", "say", "meow"], ["dog", "say", "woof"]]
        ft_model = SingleLanguageFastTextModel("hu", "hu", FastText(sentences, min_count=1))

        ft_embeddings = FastTextEmbeddings()
        #ft_model = ft_embeddings.load_embeddings_by_id(args.lang)

        certificates = clef_data.extend_certificates_by_dictionaries(certificates, dictionary)
        certificates = clef_data.remove_duplicates_from_certificates(certificates)
        certificates = clef_data.filter_nan_texts(certificates)

        if args.lang == "it" or args.lang == "hu":
            certificates = clef_data.split_multi_code_lines(certificates)

        certificates = clef_data.language_tag_data(certificates, "RawText", "Lang")

        certificates = clef_data.remove_duplicates_from_certificates(certificates)
        certificates = clef_data.duplicate_less_frequent(certificates, args.min_freq)

        if args.samples:
            icd10_sample = certificates["ICD10"].sample(args.samples, random_state=42).values
            sample_mask = certificates["ICD10"].isin(icd10_sample)
            certificates = certificates[sample_mask]

        dictionary = clef_data.language_tag_data(dictionary, "DiagnosisText", "Lang")

        configuration = clef18_task1.prepare_data_set(certificates, dictionary, ft_model, args.train_ratio,
                                                      args.val_ratio, args.strat_column, args.strat_splits)
        clef18_task1.save_configuration(configuration)

        neg_sampling = NegativeSampling()
        neg_sampling_strategy = neg_sampling.get_strategy_by_name(args.neg_sampling, args)

        embedding_model = clef18_task1.train_embedding_model(configuration, ft_model, args.max_pos_samples, neg_sampling_strategy,
                                                             args.epochs, args.batch_size)

        clef18_task1.train_and_evaluate_classifiers(embedding_model, configuration, args.target_labels)

    elif args.mode == "eval-cl":
        configuration = clef18_task1.reload_configuration(args.train_conf)
        embedding_model = clef18_task1.reload_embedding_model(args.emb_model)

        clef18_task1.train_and_evaluate_classifiers(embedding_model, configuration, args.target_labels)

    elif args.mode == "pred":
        embedding_model = clef18_task1.reload_embedding_model(args.emb_model)
        classifer_model = clef18_task1.reload_classifier(args.cl_model)
        configuration = clef18_task1.reload_configuration(args.train_conf)

        test_certificates = clef_data.read_test_certifcates_by_lang(args.lang)

        ft_embeddings = FastTextEmbeddings()
        ft_model = ft_embeddings.load_embeddings_by_id(args.lang)

        #sentences = [["cat", "say", "meow"], ["dog", "say", "woof"]]
        #ft_model = FastTextModel("dummy", [FastText(sentences, min_count=1)])

        clef18_task1.predict(embedding_model, classifer_model, configuration, test_certificates)

    #eval_result = clef18_task1.train_and_evaluate_classifiers(embedding_model, configuration, args.target_labels)
    #clef18_task1.save_evaluation_results(eval_result)



