from init import *
from clef18_task1_base import Clef18Task1Base, EvaluationConfiguration, NegativeSampling

import argparse
import numpy as np
import pandas as pd
import keras as k
import os

from keras import Input, Model
from keras.layers import Bidirectional, Dense, Dot, LSTM, Embedding, GlobalMaxPool1D
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from pandas import DataFrame
from sklearn.metrics import f1_score, accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from tqdm import tqdm
from typing import Tuple, Dict, List, Callable

from app_context import AppContext
from clef18_task1_data import Clef18Task1Data
from ft_embeddings import FastTextEmbeddings, FastTextModel
from preprocessing import DataPreparationUtil as pdu
from keras_extension import KerasUtil as ku


class ICD10LabelEncoders(object):

    def __init__(self, chapter_encoder: LabelEncoder, section_encoder: LabelEncoder,
                 subsection_encoder: LabelEncoder, code_encoder: LabelEncoder):
        self.chapter_encoder = chapter_encoder
        self.section_encoder = section_encoder
        self.subsection_encoder = subsection_encoder
        self.code_encoder = code_encoder


class Configuration(object):

    def __init__(self, train_df: DataFrame, val_df: DataFrame, test_df: DataFrame, max_length: int, ft_embedding_size: int,
                 label_column: str, label_encoders: ICD10LabelEncoders, keras_tokenizer: Tokenizer):
        self.train_df = train_df
        self.val_df = val_df
        self.test_df = test_df
        self.max_length = max_length
        self.ft_embedding_size = ft_embedding_size
        self.label_column = label_column
        self.label_encoders = label_encoders
        self.keras_tokenizer = keras_tokenizer


class EvaluationResult(object):

    def __init__(self, target_label: str, classifier_name: str, data_set_name: str, accuracy: float):
        self.target_label = target_label
        self.classifier_name = classifier_name
        self.data_set_name = data_set_name
        self.accuracy = accuracy


class Clef18Task1Emb2(Clef18Task1Base):

    def __init__(self):
        Clef18Task1Base.__init__(self)

    def train_embedding_model(self, config: Configuration, ft_model: FastTextModel, neg_sampling_strategy: Callable, epochs: int, batch_size: int) -> Model:
        self.logger.info("Start building training pairs")
        train_pair_data = self.build_pairs(config.train_df, neg_sampling_strategy)
        self.logger.info("Label distribution:\n%s", train_pair_data["Label"].value_counts())

        self.logger.info("Start building embedding model")
        model = self.build_embedding_model(config.keras_tokenizer.word_index, ft_model, config)
        model.summary(print_fn=self.logger.info)

        cert_inputs = pad_sequences(train_pair_data["Cert_input"].values, maxlen=config.max_length, padding="post")
        icd10_inputs = train_pair_data["ICD10_input"].values
        labels = train_pair_data["Label"].values

        self.logger.info("Start training of embedding model")
        best_model_file = os.path.join(AppContext.default().output_dir, "embedding_model_best.h5")

        if config.val_df is not None and len(config.test_df) > 0:
            self.logger.info("Start creation of validation pairs")
            val_pair_data = self.build_pairs(config.val_df, neg_sampling_strategy)

            val_cert_inputs = pad_sequences(val_pair_data["Cert_input"].values, maxlen=config.max_length, padding="post")
            val_icd10_inputs = val_pair_data["ICD10_input"].values
            val_gold_labels = val_pair_data["Label"].values

            model.fit([cert_inputs, icd10_inputs], labels, epochs=epochs, batch_size=batch_size,
                      validation_data=([val_cert_inputs, val_icd10_inputs], val_gold_labels),
                      callbacks=[ku.best_model_checkpointing_by_file_path(best_model_file, "val_loss")])
        else:
            model.fit([cert_inputs, icd10_inputs], labels, epochs=epochs, batch_size=batch_size,
                    callbacks=[ku.best_model_checkpointing_by_file_path(best_model_file)])

        model_file = os.path.join(AppContext.default().output_dir, "embedding_model_last.h5")
        self.logger.info("Saving last model to %s", model_file)
        model.save(model_file)

        self.logger.info("Reloading best embedding model from %s", best_model_file)
        model = k.models.load_model(best_model_file)

        ## ----------------------------------------------------------------------------------------------------------

        if config.val_df is not None and len(config.val_df) > 0:
            self.logger.info("Start evaluation of embedding model!")

            self.logger.info("Start creation of test pairs")
            test_pair_data = self.build_pairs(config.test_df, neg_sampling_strategy)

            test_cert_inputs = pad_sequences(test_pair_data["Cert_input"].values, maxlen=config.max_length, padding="post")
            test_icd10_inputs = test_pair_data["ICD10_input"].values
            test_gold_labels = test_pair_data["Label"].values

            self.logger.info("Start prediction of test labels")
            pred_labels = model.predict([test_cert_inputs, test_icd10_inputs], verbose=1)
            pred_labels = (pred_labels > 0.5).astype(float)

            f1_value = f1_score(test_gold_labels, pred_labels)
            acc_value = accuracy_score(test_gold_labels, pred_labels)

            self.logger.info("Result: f1_score= %s | acc_score= %s", f1_value, acc_value)

        return model

    def train_and_evaluate_classifiers(self, emb_model: Model, config: Configuration, target_labels: List) -> List[EvaluationResult]:
        self.logger.info("Start training and evaluation of classifier models")

        self.logger.info("Building embeddings for training data")
        text_input = emb_model.inputs[0]
        text_rnn = Model(inputs=text_input, outputs=emb_model.get_layer("text_rnn").output, name="Cert-RNN-Model")

        train_inputs = pad_sequences(config.train_df["Token_ids"].values, maxlen=config.max_length)
        train_embeddings = text_rnn.predict(train_inputs, verbose=1)
        self.logger.info("cert train input shape: %s", train_embeddings.shape)

        val_inputs = pad_sequences(config.val_df["Token_ids"].values, maxlen=config.max_length)
        val_embeddings = text_rnn.predict(val_inputs, verbose=1)
        self.logger.info("cert val input shape: %s", val_embeddings.shape)

        test_inputs = pad_sequences(config.test_df["Token_ids"].values, maxlen=config.max_length)
        test_embeddings = text_rnn.predict(test_inputs, verbose=1)
        self.logger.info("cert test input shape: %s", test_embeddings.shape)

        target_label_configs = self.get_label_configuration(target_labels, config.label_encoders)

        input_dim = text_rnn.output.shape[1].value

        eval_config = EvaluationConfiguration(target_labels, config.label_encoders, input_dim, train_embeddings, config.train_df,
                                              val_embeddings, config.val_df, test_embeddings, config.test_df)
        return self.run_evaluation(eval_config)

    def build_embedding_model(self, word_index: Dict, ft_model: FastTextModel, conf: Configuration):
        # TODO: Make hyper-parameter configurable!
        # TODO: Think about using CNNs instead of RNNs!

        embedding_matrix = np.zeros((len(word_index) + 1, ft_model.vector_size()))
        for word, i in word_index.items():
            try:
                embedding_vector = ft_model.lookup(word)
                if embedding_vector is not None:
                    # words not found in embedding index will be all-zeros.
                    embedding_matrix[i] = embedding_vector
            except KeyError:
                self.logger.error("Can't create embedding for '%s'", word)

        input_embedding = Embedding(len(word_index)+1, ft_model.vector_size(), weights=[embedding_matrix], mask_zero=True)

        # Model 1: Learn a representation of a line originating from a death certificate or the dictionary
        input_certificate_line = Input((conf.max_length, ))
        cert_embeddings = input_embedding(input_certificate_line)
        certificate_rnn = Bidirectional(LSTM(200), name="text_rnn")(cert_embeddings)

        # Model 2: Learn a representation of a ICD-10 code
        num_icd10_codes = len(conf.label_encoders.code_encoder.classes_)

        icd10_input = Input((1, ))
        icd10_embedding = Embedding(num_icd10_codes, 400, mask_zero=False)(icd10_input)
        icd10_embedding = GlobalMaxPool1D()(icd10_embedding)

        # Calculate similarity between both representations
        dot_product = Dot(axes=1, normalize=True)([certificate_rnn, icd10_embedding])

        output = Dense(1, activation='sigmoid')(dot_product)

        # Create the primary training model
        model = Model(inputs=[input_certificate_line, icd10_input], outputs=output, name="ICD10-Embedding-Model2")
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=["accuracy"])

        return model

    def prepare_data_set(self, cert_df: DataFrame, dict_df: DataFrame, ft_model: FastTextModel, train_ratio: float, val_ratio: float,
                         strat_column: str, samples: int=None, stratified_splits: bool=False) -> Configuration:

        cert_df = cert_df[["RawText", "ICD10"]]
        cert_df.columns = ["Text", "ICD10"]

        dict_df = dict_df[["DiagnosisText", "ICD10"]]
        dict_df.columns = ["Text", "ICD10"]

        complete_df = pd.concat([cert_df, dict_df])
        self.logger.info("Concatenated certificate and dictionary entries. Found %s in total.", len(complete_df))

        if samples:
            self.logger.info("Sampling %s instances", samples)
            complete_df = complete_df.sample(samples, random_state=42)

        self.logger.info("Splitting certificate lines into train and evaluation data set")
        train_df, evaluation_df = self.split_train_test(complete_df, train_ratio, stratified_splits, strat_column)
        self.logger.info("Finished splitting: train=%s instances, evaluation=%s instances", len(train_df), len(evaluation_df))

        self.logger.info("Splitting evaluation data set into validation and test set")
        val_df, test_df = self.split_train_test(evaluation_df, val_ratio, stratified_splits, strat_column)

        label_encoders = self.prepare_label_encoders(dict_df, cert_df)
        keras_tokenizer = Tokenizer(oov_token="<UNK>")

        self.logger.info("Start preparation of training data (%s instances)", len(train_df))
        train_df, max_length = self.prepare_cert_dict_df(train_df, "train", label_encoders, keras_tokenizer)

        self.logger.info("Start preparation of validation data (%s instances)", len(val_df))
        val_df, _ = self.prepare_cert_dict_df(val_df, "validation", label_encoders, keras_tokenizer)

        self.logger.info("Start preparation of test cert data (%s instances)", len(test_df))
        test_df, _ = self.prepare_cert_dict_df(test_df, "test", label_encoders, keras_tokenizer)

        return Configuration(train_df, val_df, test_df, max_length, ft_model.vector_size(),
                             strat_column, label_encoders, keras_tokenizer)

    def prepare_cert_dict_df(self, cert_dict_df: DataFrame, mode: str, icd10_encoders: ICD10LabelEncoders,
                             keras_tokenizer: Tokenizer) -> Tuple[DataFrame, int]:
        pipeline = Pipeline([
            ("Extract-ICD10-chapter", pdu.extract_icd10_chapter("ICD10", "ICD10_chapter")),
            ("Encode-ICD10-chapter", pdu.encode_labels("ICD10_chapter", "ICD10_chapter_encoded",
                                                       icd10_encoders.chapter_encoder, False)),

            ("Extract-ICD10-section", pdu.extract_icd10_section("ICD10", "ICD10_section")),
            ("Encode-ICD10-section", pdu.encode_labels("ICD10_section", "ICD10_section_encoded",
                                                       icd10_encoders.section_encoder, False)),

            ("Extract-ICD10-subsection", pdu.extract_icd10_subsection("ICD10", "ICD10_subsection")),
            ("Encode-ICD10-subsection", pdu.encode_labels("ICD10_subsection", "ICD10_subsection_encoded",
                                                       icd10_encoders.subsection_encoder, False)),

            ("Clean-ICD10-code", pdu.strip("ICD10")),
            ("Encode-ICD10-code", pdu.encode_labels("ICD10", "ICD10_encoded",
                                                     icd10_encoders.code_encoder, False)),

            ("LowercaseText", pdu.to_lowercase("Text")),
            ("TokenizeText", pdu.keras_sequencing("Text", "Token_ids", keras_tokenizer, (mode == "train")))
        ])

        data_prepared = pipeline.fit_transform(cert_dict_df)

        if mode == "train":
            max_length = max([len(array) for array in data_prepared["Token_ids"].values])
        else:
            max_length = None

        return data_prepared, max_length

    def build_pairs(self, data_df: DataFrame, neg_sampling_strategy: Callable):
        # TODO: Think about to use a negative sample ratio (depending on true dictionary entries), e.g. 0.5 or 1.2

        text_vectors = []
        icd10_codes = []
        labels = []

        for i, data_row in tqdm(data_df.iterrows(), desc="build-pairs", total=len(data_df)):
            # Build positive sample (based on training data)
            text_vectors.append(data_row["Token_ids"])
            icd10_codes.append(data_row["ICD10_encoded"])
            labels.append(1.0)

            # Build negative samples
            negative_samples = neg_sampling_strategy(data_df, data_row["ICD10"])

            for i, neg_row in negative_samples.iterrows():
                text_vectors.append(data_row["Token_ids"])
                icd10_codes.append(neg_row["ICD10_encoded"])

                labels.append(0.0)

        data = {"Cert_input": text_vectors, "ICD10_input": icd10_codes, "Label": labels}
        return pd.DataFrame(data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="CLEF2018")
    subparsers = parser.add_subparsers(dest="mode")

    train_emb_parser = subparsers.add_parser("train-emb")
    train_emb_parser.add_argument("lang", help="Language to train on", choices=["it", "fr", "hu", "all-con"])
    train_emb_parser.add_argument("--epochs", help="Number of epochs to train", default=10, type=int)
    train_emb_parser.add_argument("--batch_size", help="Batch size during training", default=10, type=int)
    train_emb_parser.add_argument("--train_ratio", help="Ratio of samples (from the complete data set) to use for training", default=0.8, type=float)
    train_emb_parser.add_argument("--val_ratio", help="Ratio of samples (from the evaluation data set) to use for validation", default=0.3, type=float)

    train_emb_parser.add_argument("--samples", help="Number of instances to sample from the (original) training data", default=None, type=int)
    train_emb_parser.add_argument("--strat_column", help="Column used to stratify the data sets", default="ICD10_masked", type=str)
    train_emb_parser.add_argument("--strat_splits", help="Indicates whether to use stratified sampling", default=False, type=bool)
    train_emb_parser.add_argument("--target_labels", help="Target columns for the classification models", default=["icd10"], action='append')

    train_emb_parser.add_argument("--neg_sampling", help="Negative sampling strategy to use", default="ext1", choices=["def", "ext1"])
    train_emb_parser.add_argument("--num_neg_samples", help="Number of negative samples to use (default strategy)", default=75, type=int)
    train_emb_parser.add_argument("--num_neg_cha", help="Number of negative chapter samples to use (ext1 strategy)", default=20, type=int)
    train_emb_parser.add_argument("--num_neg_sec", help="Number of negative section samples to use (ext1 strategy)", default=20, type=int)
    train_emb_parser.add_argument("--num_neg_sub", help="Number of negative subsection samples to use (ext1 strategy)", default=20, type=int)
    train_emb_parser.add_argument("--num_neg_oth", help="Number of negative other samples to use (ext1 strategy)", default=45, type=int)

    eval_classifier_parser = subparsers.add_parser("eval-cl")
    eval_classifier_parser.add_argument("emb_model", help="Path to the embedding model to use")
    eval_classifier_parser.add_argument("train_conf", help="Path to the training configuration dump")
    eval_classifier_parser.add_argument("lang", help="Language to train on", choices=["it", "fr", "hu"])
    eval_classifier_parser.add_argument("--train_ratio", help="Ratio of samples (from the complete data set) to use for training", default=0.8, type=float)
    eval_classifier_parser.add_argument("--val_ratio", help="Ratio of samples (from the evaluation data set) to use for validation", default=0.4, type=float)
    eval_classifier_parser.add_argument("--samples", help="Number of instances to sample from the (original) training data", default=None, type=int)
    eval_classifier_parser.add_argument("--strat_column", help="Column used to stratify the data sets", default="ICD10_masked", type=str)
    eval_classifier_parser.add_argument("--strat_splits", help="Indicates whether to use stratified sampling", default=False, type=bool)
    eval_classifier_parser.add_argument("--target_labels", help="Target columns for the classification models", default=["icd10"], action='append')

    args = parser.parse_args()

    AppContext.initialize_by_app_name(Clef18Task1Emb2.__name__ + "-" + args.mode)

    clef_data = Clef18Task1Data()
    dictionary = clef_data.read_dictionary_by_id(args.lang)
    #dictionary = dictionary.sample(1200)

    certificates = clef_data.read_train_certifcates_by_id(args.lang)
    #certificates = clef_data.filter_single_code_lines(certificates)
    #certificates = clef_data.add_masked_icd10_column(certificates, 10)

    sentences = [["cat", "say", "meow"], ["dog", "say", "woof"]]
    #ft_model = FastText(sentences, min_count=1)

    ft_embeddings = FastTextEmbeddings()
    ft_model = ft_embeddings.load_embeddings_by_id(args.lang)

    clef18_task1 = Clef18Task1Emb2()
    clef18_task1.save_arguments(args)

    if args.mode == "train-emb":
        configuration = clef18_task1.prepare_data_set(certificates, dictionary, ft_model, args.train_ratio, args.val_ratio,args.strat_column,
                                                      args.samples, args.strat_splits)
        clef18_task1.save_configuration(configuration)

        neg_sampling = NegativeSampling()
        neg_sampling_strategy = neg_sampling.get_strategy_by_name(args.neg_sampling, args)

        embedding_model = clef18_task1.train_embedding_model(configuration, ft_model, neg_sampling_strategy, args.epochs, args.batch_size)

    elif args.mode == "eval-cl":
        configuration = clef18_task1.reload_configuration(args.train_conf)
        embedding_model = clef18_task1.reload_embedding_model(args.emb_model)

    eval_result = clef18_task1.train_and_evaluate_classifiers(embedding_model, configuration, args.target_labels)
    clef18_task1.save_evaluation_results(eval_result)

