from typing import List

from keras import Sequential
from keras.initializers import VarianceScaling
from keras.layers import Dense, Dropout, BatchNormalization
from keras.optimizers import Adam
from keras.wrappers.scikit_learn import KerasClassifier

from keras_extension import ExtendedKerasClassifier


class NeuralNetworkClassifiers(object):

    @staticmethod
    def dense_network(input_dim: int, output_dim: int, hidden_layer_sizes: List[int], batch_normalization: bool,
                      dropout_rate: float, epochs: int, batch_size: int, callbacks: List = None):
        def _build_model():
            model = Sequential()

            for i, layer_size in enumerate(hidden_layer_sizes):
                if i == 0:
                    model.add(Dense(layer_size, input_shape=(input_dim,), kernel_initializer=VarianceScaling(), activation="selu"))
                else:
                    model.add(Dense(layer_size, kernel_initializer=VarianceScaling(), activation="selu"))

                if batch_normalization:
                    model.add(BatchNormalization())

                if dropout_rate and dropout_rate > 0.0:
                    model.add(Dropout(dropout_rate))

            model.add(Dense(output_dim, activation="softmax"))
            model.compile(optimizer=Adam(), loss="sparse_categorical_crossentropy", metrics=['accuracy'])

            return model

        #return KerasClassifier(_build_model, epochs=epochs, batch_size=batch_size, callbacks=callbacks)
        return ExtendedKerasClassifier(_build_model, epochs=epochs, batch_size=batch_size, callbacks=callbacks)
