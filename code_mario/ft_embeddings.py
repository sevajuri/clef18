import numpy as np

from typing import List, Dict
from gensim.models import FastText

from app_context import AppContext
from common_util import LoggingMixin


class FastTextModel(LoggingMixin):

    def __init__(self, name: str):
        LoggingMixin.__init__(self, self.__class__.__name__, AppContext.default().default_log_file)
        self.name = name

    def lookup(self, word: str, language: str):
        raise NotImplementedError()

    def vector_size(self):
        raise NotImplementedError()


class SingleLanguageFastTextModel(FastTextModel):

    def __init__(self, name: str, lang: str, ft_model: FastText):
        FastTextModel.__init__(self, name)
        self.name = name
        self.lang = lang.lower()
        self.ft_model = ft_model

    def lookup(self, word: str, lang: str):
        embeddings = []
        if self.lang == lang.lower():
            try:
                return  embeddings.append(self.ft_model[word])
            except KeyError as error:
                self.logger.warn("Can't create embedding for " + word)
                return np.zeros(self.ft_model.vector_size)
        else:
            self.logger.warn("FastText model doesn't support language %s", lang)
            return np.zeros(self.ft_model.vector_size)

    def vector_size(self):
        return self.ft_model.vector_size


class MultiLanguageFastTextModel(FastTextModel):

    def __init__(self, name: str, ft_models: Dict[str, FastText]):
        FastTextModel.__init__(self, name)
        self.name = name
        self.ft_models = ft_models
        self.emb_size = ft_models.values()[0].vector_size

    def lookup(self, word: str, lang: str):
        lang = lang.lower()

        if lang in self.ft_models:
            ft_model = self.ft_models[lang]

            try:
                return ft_model[word]
            except KeyError as error:
                self.logger.warn("Can't create embedding for " + word)
                return np.zeros(ft_model.vector_size)
        else:
            self.logger.warn("FastText model doesn't support language %s", lang)
            return np.zeros(self.emb_size)

    def vector_size(self):
        return self.emb_size


class MultiLanguageConcatenationFastTextModel(FastTextModel):

    def __init__(self, name: str, ft_models: List[FastText]):
        FastTextModel.__init__(self, name)
        self.name = name
        self.ft_models = ft_models
        self.emb_size = sum([ft_model.vector_size for ft_model in ft_models])

    def lookup(self, word: str, lang: str):
        embeddings = []
        for ft_model in self.ft_models:
            try:
                embeddings.append(ft_model[word])
            except KeyError:
                self.logger.warn("Can't create embedding for " + word)
                embeddings.append(np.zeros(ft_model.vector_size))

        if len(embeddings) == 1:
            return embeddings[0]
        else:
            return np.concatenate(embeddings)

    def vector_size(self):
        return self.emb_size


class FastTextEmbeddings(LoggingMixin):

    def __init__(self):
        LoggingMixin.__init__(self, self.__class__.__name__, AppContext.default().default_log_file)

    def load_embeddings_by_id(self, id: str) -> FastTextModel:
        if id == "it":
            return SingleLanguageFastTextModel("it", "it", self.load_it_embeddings())

        elif id == "hu":
            return SingleLanguageFastTextModel("hu", "hu", self.load_hu_embeddings())

        elif id == "fr":
            return SingleLanguageFastTextModel("fr", "fr", self.load_fr_embeddings())

        elif id == "multi":
            return MultiLanguageFastTextModel("multi", {
                "it" : self.load_it_embeddings(),
                "hu" : self.load_hu_embeddings(),
                "fr" : self.load_fr_embeddings()
            })

        elif id == "all-con":
            return MultiLanguageConcatenationFastTextModel("all-con", [self.load_fr_embeddings(),
                                                                       self.load_it_embeddings(),
                                                                       self.load_hu_embeddings()])

        else:
            raise AssertionError("Unsupported language: " + id)

    # ------------------------------------------------------------------------------------

    def load_fr_embeddings(self) -> FastText:
        french_ft_file = "../code_jurica/data/embeddings/cc.fr.300.bin"
        return self._load_ft_model(french_ft_file)

    def load_hu_embeddings(self) -> FastText:
        hungarian_it_file = "../code_jurica/data/embeddings/cc.hu.300.bin"
        return self._load_ft_model(hungarian_it_file)

    def load_it_embeddings(self) -> FastText:
        italian_ft_file = "../code_jurica/data/embeddings/cc.it.300.bin"
        return self._load_ft_model(italian_ft_file)

    def _load_ft_model(self, ft_file: str) -> FastText:
        self.logger.info("Loading fast text embeddings from %s", ft_file)
        ft_model = FastText.load_fasttext_format(ft_file)
        self.logger.info("Finished loading of fast text model")

        return ft_model
