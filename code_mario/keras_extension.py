import os
import keras as k

from logging import Logger
from keras.callbacks import Callback, ModelCheckpoint, CSVLogger, EarlyStopping
from keras.wrappers.scikit_learn import KerasClassifier

from app_context import AppContext
from common_util import LoggingMixin


class KerasUtil(object):

    @staticmethod
    def best_model_checkpointing_by_model_name(model_name: str, monitor_loss: str = "loss"):
        models_dir = os.path.join(AppContext.default().output_dir, "models")
        os.makedirs(models_dir, exist_ok=True)

        best_model_file = os.path.join(models_dir, "%s_best.h5" % model_name)
        return ModelCheckpoint(filepath=best_model_file, monitor=monitor_loss, save_best_only=True, verbose=1)

    @staticmethod
    def best_model_checkpointing_by_file_path(best_model_file: str, monitor_loss: str = "loss"):
        return ModelCheckpoint(filepath=best_model_file, monitor=monitor_loss, save_best_only=True, verbose=1)

    @staticmethod
    def early_stopping(monitor_loss: str, patience: int):
        return EarlyStopping(monitor_loss, patience=patience, verbose=1)

    @staticmethod
    def csv_logging_callback(model_name: str, label: str):
        train_log_dir = os.path.join(AppContext.default().log_dir, "train_logs")
        try:
            os.makedirs(train_log_dir, exist_ok=True)
        except:
            print("Can't create train log directory: " + train_log_dir)

        log_file_name = "%s_%s.log" % (model_name, label)
        training_log_file = os.path.join(train_log_dir, log_file_name)

        return CSVLogger(training_log_file, separator=";", append=True)


class LoggerCallback(Callback):

    def __init__(self, logger: Logger):
        Callback.__init__(self)
        self.logger = logger

    def on_epoch_end(self, epoch, logs=None):
        metrics = dict()
        for k in self.params['metrics']:
            if k in logs:
                metrics[k] = logs[k]

        self.logger.debug("Finished epoch %s (%s)", epoch, metrics)


class ExtendedKerasClassifier(KerasClassifier, LoggingMixin):

    def __init__(self, build_fn=None, **sk_params):
        super(ExtendedKerasClassifier, self).__init__(build_fn, **sk_params)
        LoggingMixin.__init__(self, self.__class__.__name__, AppContext.default().default_log_file)
        self.re_fitted = False

    def fit(self, x, y, sample_weight=None, **kwargs):
        self.re_fitted = True
        return super(ExtendedKerasClassifier, self).fit(x, y, sample_weight, **kwargs)

    def predict(self, x, **kwargs):
        if self.re_fitted:
            if "callbacks" in self.sk_params:
                checkpoint_callbacks = [callback for callback in self.sk_params["callbacks"]
                                        if isinstance(callback, ModelCheckpoint) and callback.save_best_only]
                if checkpoint_callbacks:
                    #self.logger.debug("Reloading model from %s", checkpoint_callbacks[0].filepath)
                    self.model = k.models.load_model(checkpoint_callbacks[0].filepath)
                    self.re_fitted = False
                else:
                    self.logger.debug("Can't find best-model-checkpoint-callback.")
            else:
                self.logger.debug("Can't find callbacks parameter. No callbacks configured?")
        else:
            #self.logger.debug("Model wasn't re-fitted -> re-using existing model")
            pass

        #self.logger.debug("Classifer has %s classes", len(self.classes_))
        return super(ExtendedKerasClassifier, self).predict(x, **kwargs)

    def __getstate__(self):
        dict_copy = self.__dict__.copy()
        del dict_copy["logger"]
        return dict_copy

    def __setstate__(self, state):
        super(LoggingMixin, self).__init__(self.__class__.__name__, AppContext.default().default_log)




