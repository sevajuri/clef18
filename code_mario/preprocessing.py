import re
import numpy as np

from gensim.models import FastText
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from pandas import DataFrame
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder
from typing import Callable, List

from common_util import PandasUtil


class DataPreparationUtil(object):

    @staticmethod
    def to_lowercase(column: str):
        def _lower(text):
            return str(text).lower()

        return MapFunction(column, _lower)

    @staticmethod
    def strip(column: str):
        def _strip(text):
            return str(text).strip()

        return MapFunction(column, _strip)

    @staticmethod
    def tokenize(text_column: str, token_column: str = "tokens"):
        return SimpleTokenizer(text_column, token_column)

    @staticmethod
    def keras_sequencing(text_column: str, target_column: str, tokenizer: Tokenizer, fit_tokenizer: bool):
        return KerasSequencer(text_column, target_column, tokenizer, fit_tokenizer)

    @staticmethod
    def count_values(column: str, target_column: str):
        def _count(value):
            return len(value)
        return MapFunction(column, _count, target_column)

    @staticmethod
    def lookup_fast_text_vectors(token_column: str, target_column: str, ft_model: FastText):
        return FastTextVectorsLookup(token_column, target_column, ft_model)

    @staticmethod
    def vectors_to_matrix(vectors_column: str, matrix_column: str, vector_size: int, max_length: int = None):
        return VectorListToMatrix(vectors_column, matrix_column, vector_size, max_length)

    @staticmethod
    def combine_texts(columns: List[str], target_column: str):
        return CombineTexts(columns, target_column)

    @staticmethod
    def encode_labels(label_column: str, target_column, label_encoder: LabelEncoder = None, fit_encoder: bool = True):
        return LabelEncoderWrapper(label_column, target_column, label_encoder, fit_encoder)

    @staticmethod
    def extract_icd10_chapter(icd10_column: str, target_column: str):
        def _extract(value):
            return value.strip()[0].lower()

        return MapFunction(icd10_column, _extract, target_column)

    @staticmethod
    def extract_icd10_section(icd10_column: str, target_column: str):
        def _extract(value):
            return value.strip()[0:2].lower()

        return MapFunction(icd10_column, _extract, target_column)

    @staticmethod
    def extract_icd10_subsection(icd10_column: str, target_column: str):
        def _extract(value):
            return value.strip()[0:3].lower()

        return MapFunction(icd10_column, _extract, target_column)

    @staticmethod
    def mask_icd10(icd10_column:str, target_column:str, value_counts, min_support: int, icd10_mask:str):
        def _mask_icd10(icd10):
            if str(icd10) in value_counts and value_counts[str(icd10)] >= min_support:
                    return icd10

            return icd10_mask

        return MapFunction(icd10_column, _mask_icd10, target_column)

    @staticmethod
    def clean_text(column:str):
        def _clean(value):
            return str(value).replace("\"", " ")

        return MapFunction(column, _clean)

    @staticmethod
    def tag_words_with_language(text_column: str, target_column: str, lang_column: str):
        return LanguageWordTagger(text_column, target_column, lang_column)


class FitMixin(object):

    def fit(self, data, y=None):
        return self


class MapFunction(BaseEstimator, TransformerMixin, FitMixin):

    def __init__(self, column: str, map_function: Callable, target_column: str = None):
        self.column = column
        self.map_function = map_function

        if target_column:
            self.target_column = target_column
        else:
            self.target_column = column

    def transform(self, data: DataFrame, y=None):
        values = data[self.column].apply(self.map_function)
        return PandasUtil.append_column(data, self.target_column, values)


class FastTextVectorsLookup(BaseEstimator, TransformerMixin, FitMixin):

    def __init__(self, token_column: str, vector_column: str, ft_model: FastText):
        self.token_column = token_column
        self.vector_column = vector_column
        self.ft_model = ft_model

    def transform(self, data: DataFrame, y= None) -> DataFrame:
        def _lookup_vectors(tokens):
            vectors = []
            for token in tokens:
                try:
                    vectors.append(self.ft_model[token])
                except KeyError:
                    print("Can't create embedding for "+token)
            return vectors

        vectors = data[self.token_column].apply(_lookup_vectors)
        return PandasUtil.append_column(data, self.vector_column, vectors)


class SimpleTokenizer(BaseEstimator, TransformerMixin, FitMixin):

    def __init__(self, text_column: str, token_column: str):
        self.text_column = text_column
        self.token_column = token_column

    def transform(self, data: DataFrame, y= None) -> DataFrame:
        def _tokenize(value):
            return [value.strip() for value in re.split("[ ,.=\t\!]", str(value)) if value.strip()]

        tokens = data[self.text_column].apply(_tokenize)
        return PandasUtil.append_column(data, self.token_column, tokens)


class CombineTexts(BaseEstimator, TransformerMixin, FitMixin):

    def __init__(self, text_columns: List[str], target_column: str, joiner: str = " "):
        self.text_columns = text_columns
        self.target_column = target_column
        self.joiner = joiner

    def transform(self, data: DataFrame, y=None):
        def _combine(row):
            return self.joiner.join([str(row[column]).strip() for column in self.text_columns if str(row[column]).strip()])

        combined_texts = data.apply(_combine, axis=1)
        return PandasUtil.append_column(data, self.target_column, combined_texts)


class VectorListToMatrix(BaseEstimator, TransformerMixin, FitMixin):

    def __init__(self, vector_column: str, matrix_column: str, vector_size: int, max_length: int = None):
        self.vector_column = vector_column
        self.matrix_column = matrix_column
        self.vector_size = vector_size
        self.max_length = max_length

    def transform(self, data: DataFrame, y=None):
        if not self.max_length:
            self.max_length = max([len(value) for value in data[self.vector_column].values])

        matrices = data[self.vector_column].apply(self._build_matrix())
        return PandasUtil.append_column(data, self.matrix_column, matrices)

    def _build_matrix(self):
        def _build(vectors):
            matrix = np.zeros((self.max_length, self.vector_size))
            for i in range(self.max_length):
                if i == len(vectors):
                    break

                matrix[i] = vectors[i]

            return matrix

        return _build


class LabelEncoderWrapper(BaseEstimator, TransformerMixin):

    def __init__(self, label_column: str, target_column: str, label_encoder: LabelEncoder=None, fit_encoder: bool=True):
        self.label_column = label_column
        self.target_column = target_column
        self.fit_encoder = fit_encoder

        if label_encoder:
            self.label_encoder = label_encoder
        else:
            self.label_encoder = LabelEncoder()
            self.fit_encoder = True  #FIXME: Make this sense?

    def fit(self, data: DataFrame, y=None):
        if self.fit_encoder:
            self.label_encoder.fit(data[self.label_column])
        return self

    def transform(self, data: DataFrame, y=None):
        encoded_labels = self.label_encoder.transform(data[self.label_column])
        return PandasUtil.append_column(data, self.target_column, encoded_labels)


class KerasSequencer(BaseEstimator, TransformerMixin):

    def __init__(self, text_column: str, target_column: str, keras_tokenizer: Tokenizer, fit_tokenizer: bool):
        self.text_column = text_column
        self.target_column = target_column
        self.keras_tokenizer = keras_tokenizer
        self.fit_tokenizer = fit_tokenizer

    def fit(self, data: DataFrame, y=None):
        if self.fit_tokenizer:
            texts = data[self.text_column].values
            self.keras_tokenizer.fit_on_texts(texts)

        return self

    def transform(self, data: DataFrame, y=None):
        texts = data[self.text_column].astype(str).values
        sequences = self.keras_tokenizer.texts_to_sequences(texts)

        return PandasUtil.append_column(data, self.target_column, sequences)


class LanguageWordTagger(BaseEstimator, FitMixin, TransformerMixin):

    def __init__(self, text_column: str, target_column: str, lang_column: str):
        self.text_column = text_column
        self.target_column = target_column
        self.lang_column = lang_column

    def transform(self, data: DataFrame, y=None):
        def _tag_words(row):
            language = row[self.lang_column]
            result = " ".join("%s%s" % (language, word) for word in text_to_word_sequence(str(row[self.text_column])))
            return result

        data[self.target_column] = data.apply(_tag_words, axis=1)
        return data
